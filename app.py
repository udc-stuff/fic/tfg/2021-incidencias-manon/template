import config
from app import get_app

app = get_app()
app.run()
