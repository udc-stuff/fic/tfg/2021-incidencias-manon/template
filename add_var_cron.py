import os
from crontab import CronTab

env_list = ["CREATE_ENTITIES", "DATABASE_REQUIRED", "DATABASE_USERNAME", "DATABASE_PASSWORD", "DATABASE_SERVER",
            "DATABASE_PORT", "DATABASE_DATABASE", "DATABASE_QUERY_CONFIGURATION", "DATABASE_PROTOCOL", "CELERY_BROKER",
            "CELERY_BACKEND"]
env_str = ""

for k, v in sorted(os.environ.items()):
    if k in env_list:
        aux = k+'='+v
        env_str += aux + ' '

my_cron = CronTab(user=True)

for job in my_cron:
    print(job)

command = job.command
index = command.find('FLASK_APP=app.py')
command = command[:index] + env_str + command[index:]
index = command.find('cd ')
command = command[:index] + "(" + command[index:]

job.set_comment("")
job.set_command(command + ") >> /tmp/crontab.log 2>&1")

my_cron.write()
