# User project

This is a project is to have a microservice to manage stuffs about users. 

## Brief Description of the Application Structure

- **docs**: in this folder you could place project related documentation files
- **tests**: in this folder contains your tests
- **app**: contains the application itself
- **resources**: in this folder you cloud place resource files for the application
- **conf**: settings that have nothing to do with the app (e.g: nginx)
- **app.py**: this is used to run the Flask application
- **config.py**: this file contains the global configuration for the application
- **requirements.txt**: this file has a list of the required python packages for the application 


## Run application

- on local machine with development stage

```shell script
export FLASK_ENV=development
export CREATE_ENTITIES=true
export FLASK_APP=app.py
export FLASK_RUN_PORT=80
flask run --host=0.0.0.0
```

- on local machine with production stage

```shell script
export FLASK_ENV=production
export FLASK_APP=app.py
export FLASK_RUN_PORT=80
flask run --host=0.0.0.0
```

- python test

```shell script
pip install mongomock==3.20.0 
python -m unittest discover -v
```
