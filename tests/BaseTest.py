import unittest
from datetime import datetime
from typing import List
from urllib.parse import urlparse

from mongoengine import disconnect, connect
from tzlocal import get_localzone

from app import get_app, mail
from app.errors.BadRequestException import BadRequestException
from app.errors.ForbiddenException import ForbiddenException
from app.errors.NotFoundException import NotFoundException
from app.errors.NotImplementedException import NotImplementedException
from app.errors.PaymentRequiredException import PaymentRequiredException
from app.errors.ServiceUnavailableException import ServiceUnavailableException
from app.errors.UnauthorizedException import UnauthorizedException
from app.errors.UnsupportedMediaTypeException import UnsupportedMediaTypeException
from app.errors.InternalServerErrorException import InternalServerErrorException

from config import BasicConfiguration  # to avoid circular import this goes here


class BaseTest(unittest.TestCase):
    url_prefix = '/'

    def setUp(self):
        print('\n')

        # reload db connection with mongomock
        disconnect()
        connect('test', host='mongomock://localhost')

        self.app = get_app()
        self.app.testing = True

        self.app.config.update(MAIL_SUPPRESS_SEND=True)  # To do not send emails
        self.app.config.update(MAIL_DEBUG=True)  # to see logs
        self.mail = mail
        self.mail.init_app(self.app)  # reload mail configuration to get the updated mail configuration

        self.testapp = self.app.test_client()

        self.__method = {
            'get': self.testapp.get,
            'post': self.testapp.post,
            'put': self.testapp.put,
            'delete': self.testapp.delete
        }

    def tearDown(self):
        print('\n')

    def __check_response_attributes(self, url, method, json, headers, query_string, status, attr: List[str],
                                    included: bool):
        """
        make a request using _method_ and check a not found response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param attr: expected status
        :param query_string: dict with query_string (can be None)
        :param attr: list of attributes that will be checked on the response
        :param included: True check if the response has _att_, False check if the response doesn't have _att_
        :return:
        """
        result = self.__method[method.lower()](url, json=json, headers=headers, query_string=query_string)
        data = result.json

        self.assertEqual(result.status_code, status)
        self.assertEqual(result.content_type, 'application/json')
        for item in attr:
            if included:
                self.assertIn(item, data)
            else:
                self.assertNotIn(item, data)

        return result

    def check_response_no_have_attributes(self, url, method, json, headers, query_string, status, attr: List[str]):
        """
        make a request using _method_ and check a status code and _attr_ are not included on the response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :param status: expected status
        :param attr: list of expected attributes on the response
        :return:
        """
        return self.__check_response_attributes(url, method, json, headers, query_string, status, attr, False)

    def check_response_have_attributes(self, url, method, json, headers, query_string, status, attr: List[str]):
        """
        make a request using _method_ and check a status code and _attr_ are included on the response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :param status: expected status
        :param attr: list of expected attributes on the response
        :return:
        """
        return self.__check_response_attributes(url, method, json, headers, query_string, status, attr, True)

    def __check_error(self, url, method, json, headers, query_string, status, error_value, exception_value):
        """
        make a request using _method_ and check a not found response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :param status: expected status
        :param error_value: content for key error
        :param exception_value: name of the exception
        :return:
        """
        timestamp = datetime.now(get_localzone())
        result = self.__method[method.lower()](url, json=json, headers=headers, query_string=query_string)
        data = result.json

        self.assertEqual(status, result.status_code)

        if '://' in data["path"]:
            protocol_host = '{uri.scheme}://{uri.netloc}'.format(uri=urlparse(data["path"]))
        else:
            protocol_host = ''

        self.assertEqual(BasicConfiguration.VERSION, data["app_version"])
        self.assertEqual(error_value.lower(), data["error"].lower())
        self.assertEqual(exception_value.lower(), data["exception"].lower())
        tmp: str = protocol_host + url
        self.assertTrue(tmp.startswith(data["path"]))
        self.assertEqual(status, data["status"])
        # response generated after the test
        self.assertLessEqual(timestamp, datetime.fromisoformat(data["timestamp"]))
        # response generated before this assert
        self.assertGreaterEqual(datetime.now(get_localzone()), datetime.fromisoformat(data["timestamp"]))
        self.assertLess(0, len(data["trace_id"].strip()))

        return result

    def check_204(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check an OK No Content

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        result = self.__method[method.lower()](url, json=json, headers=headers, query_string=query_string)

        self.assertEqual(result.status_code, 204)
        self.assertTrue(result.content_length is None or result.content_length == 0)

        if isinstance(result.data, bytes):
            self.assertEqual(result.data, b'')
        elif isinstance(result.data, str):
            self.assertEqual(result.data, '')
        else:
            # invalid type
            self.assertTrue(False, 'Required type {}. Found: {}'.format([str, bytes], type(result.data)))

    def check_bad_request(self, url, method, json, headers, query_string, exception_value=BadRequestException.__name__):
        """
        make a request using _method_ and check a not found response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :param exception_value: expected exception
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string, 400, 'Bad Request', exception_value)

    def check_unauthorized(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check a not found response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  401, 'Unauthorized', UnauthorizedException.__name__)

    def check_payment_requirement(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check a payment requirement response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  402, 'Payment Required', PaymentRequiredException.__name__)

    def check_forbidden(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check a not found response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  403, 'Forbidden', ForbiddenException.__name__)

    def check_not_found(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check a not found response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  404, 'Not Found', NotFoundException.__name__)

    def check_method_not_allowed(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check a method not allowed response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  405, 'Method Not Allowed', 'MethodNotAllowed')

    def check_unsupported_media_type(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check an unsupported media type response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  415, 'Unsupported media type', UnsupportedMediaTypeException.__name__)

    def check_internal_server_error(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check an internal server error response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  500, 'Internal Server Error', InternalServerErrorException.__name__)

    def check_not_implemented(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check a not implemented response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  501, 'Not Implemented', NotImplementedException.__name__)

    def check_service_unavailable(self, url, method, json, headers, query_string):
        """
        make a request using _method_ and check a service unavailable response

        :param url:
        :param method: 'get' | 'post' | 'delete' | 'put'
        :param json:  dict with data (can be None)
        :param headers: dict with headers (can be None)
        :param query_string: dict with query_string (can be None)
        :return:
        """
        return self.__check_error(url, method, json, headers, query_string,
                                  503, 'Service Unavailable', ServiceUnavailableException.__name__)
