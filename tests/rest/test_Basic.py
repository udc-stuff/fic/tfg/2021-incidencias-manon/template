import unittest

from tests.BaseTest import BaseTest


class test_Basic(BaseTest):

    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_not_found(self):
        self.check_not_found('/not_found', 'get', None, None, None)


if __name__ == '__main__':    # pragma: no cover
    unittest.main()
