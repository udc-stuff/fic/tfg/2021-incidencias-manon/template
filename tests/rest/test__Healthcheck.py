import unittest

from app.domain.User import User
from app.security.AuthoritiesConstants import AuthoritiesConstants
from app.security.jwt.TokenProvider import TokenProvider
from config import SecurityConfiguration
from tests.BaseTest import BaseTest


class test_Healthcheck(BaseTest):

    security_configuration = SecurityConfiguration()

    def setUp(self):
        super().setUp()
        self.url_prefix += 'healthcheck'
        self.token_provider = TokenProvider()

    def tearDown(self):
        super().tearDown()

    def test_healthcheckWithUnauthenticatedUser(self):
        self.check_response_no_have_attributes(self.url_prefix, 'get', None, None, None, 200, ['environment'])

    def test_healthcheckWithConsumerUser(self):
        token = self.token_provider.create_token('identifier', 'admin', False, [AuthoritiesConstants.USER])

        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        self.check_response_no_have_attributes(self.url_prefix, 'get', None, headers, None, 200, ['environment'])

    def test_healthcheckWithAdminUser(self):
        u = User(login='admin', password='admin', email='admin@example.org', activated=True, langKey='gl_ES',
                 authorities=[AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN])
        u.save()

        token = self.token_provider.create_token(str(u.id), u.login, False, u.authorities)

        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        self.check_response_have_attributes(self.url_prefix, 'get', None, headers, None, 200, ['environment'])

    def test_not_healthcheck(self):
        for item in ['post', 'put', 'delete']:
            self.check_not_found(self.url_prefix, item, None, None, None)


if __name__ == '__main__':    # pragma: no cover
    unittest.main()
