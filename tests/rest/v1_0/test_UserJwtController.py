import unittest

from app.domain.User import User
from app.security.SecurityUtils import SecurityUtils
from app.services.UserService import UserService
from tests.BaseTest import BaseTest
from app.security.AuthoritiesConstants import AuthoritiesConstants


class test_AccountResource(BaseTest):

    user_service = UserService()
    security_utils = SecurityUtils()

    def setUp(self):
        super().setUp()
        self.url_prefix += 'v1.0/'
        self.url = self.url_prefix + 'authenticate'
        self.user: User = User(login='login@example.com', email='login@example.org', password='password',
                               langKey='en', activated=True)

    def tearDown(self):
        super().tearDown()

    # ############ #
    # POST account #
    # ############ #

    # no data
    def test_AuthenticateWithNoDataAndInvalidContentType(self):
        self.check_unsupported_media_type(self.url, 'post', None, None, None)

    def test_AuthenticateWithNoData(self):
        headers = {'Content-Type': 'application/json'}
        self.check_bad_request(self.url, 'post', None, headers, None)

    # empty data
    def test_AuthenticateWithEmptyData(self):
        self.check_bad_request(self.url, 'post', {}, None, None)

    # invalid data
    def test_AuthenticateWithNoRequiredData(self):
        data = {'username': 'username', 'password': 'password'}
        data.pop('username', '')
        result = self.check_bad_request(self.url, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('username' in result.json['detail'])  # required field on detail

    # invalid data
    def test_AuthenticateWithUnexpectedField(self):
        data = {'username': 'username', 'password': 'password', 'year': 2000}
        result = self.check_bad_request(self.url, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('year' in result.json['detail'])  # invalid field on detail

    def test_AuthenticatedWithNoUser(self):
        data = {'username': 'username', 'password': 'password'}
        result = self.check_bad_request(self.url, 'post', data, None, None)

        self.assertIn('username', result.json['detail'])
        self.assertIn('password', result.json['detail'])

    def test_AuthenticatedWithWrongPassword(self):
        # create an user
        self.user.save()

        tmp = {'username': 'username', 'password': 'password'}
        result = self.check_bad_request(self.url, 'post', tmp, None, None)

        self.assertIn('username', result.json['detail'])
        self.assertIn('password', result.json['detail'])

    # test with unactivated user
    def test_AuthenticateWithNoActivate(self):
        self.user.activated = False
        password = self.user.password
        self.user.set_password(password)
        self.user.save()

        # try to authenticate
        tmp = {'username': self.user.login, 'password': password}
        result = self.check_unauthorized(self.url, 'post', tmp, None, None)

        self.assertIn('activated', result.json['detail'])

    # test with role_anonymous
    def test_AuthenticateWithAnonymous(self):
        self.user.authorities = [AuthoritiesConstants.ANONYMOUS, AuthoritiesConstants.USER]
        self.user.activated = True
        password = self.user.password
        self.user.set_password(password)
        self.user.save()

        # try to authenticate
        tmp = {'username': self.user.login, 'password': password}
        result = self.check_unauthorized(self.url, 'post', tmp, None, None)

        self.assertIn('anonymous', result.json['detail'])

    def test_Authenticate(self):
        self.user.activated = True
        self.user.authorities = [AuthoritiesConstants.USER]
        password = self.user.password
        self.user.set_password(password)
        self.user.save()

        # try to authenticate
        tmp = {'username': self.user.login, 'password': password}
        result = self.testapp.post(self.url, json=tmp)
        self.assertEqual(result.status_code, 200)

        # check headers and body
        self.assertIn('Authorization', result.headers)
        self.assertIn('id_token', result.json)
        self.assertEqual(result.headers['Authorization'], 'Bearer ' + result.json['id_token'])

        # check that the token generated is valid
        self.assertTrue(self.security_utils.isAuthenticated(result.headers))


if __name__ == '__main__':    # pragma: no cover
    unittest.main()
