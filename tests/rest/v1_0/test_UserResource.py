import string
import random
import unittest
from copy import deepcopy
from datetime import datetime, timezone

from app.domain.Authority import Authority
from app.domain.User import User
from app.rest.vm.ManagedUserVm import ManagedUserVM
from app.security.SecurityUtils import SecurityUtils
from app.security.jwt.TokenProvider import TokenProvider
from app.services.UserService import UserService
from config import PageConfiguration
from tests.BaseTest import BaseTest
from app.security.AuthoritiesConstants import AuthoritiesConstants


class test_UserResource(BaseTest):

    user_service = UserService()
    security_utils = SecurityUtils()

    def setUp(self):
        super().setUp()
        self.url_prefix += 'v1.0/users'

        self.user_vm_json: dict = {
            'login': 'loginname',
            'password': 's3cr3t',
            'firstName': 'name',
            'lastName': 'surname',
            'email': 'loginname@example.com',
            'activated': False,
            'langKey': 'gl_ES',
            'authorities': [AuthoritiesConstants.USER]
        }
        self.user = User(**self.user_vm_json)

        self.token_provider = TokenProvider()
        self.admin_token = self.token_provider.create_token('id', self.user_vm_json['login'], False,
                                                            [AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN])
        self.admin_headers = {
            TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + self.admin_token
        }

        tmp = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))

        self.id_system = 'id-system-' + tmp
        self.login_system = 'system-' + tmp + '@example.org'
        self.managed_system_user = ManagedUserVM(login=self.login_system, password='987654', first_name='t',
                                                 lang_key='en',
                                                 last_name='system', email=self.login_system, activated=True,
                                                 authorities=[AuthoritiesConstants.USER,
                                                              AuthoritiesConstants.ADMIN, AuthoritiesConstants.SYSTEM])
        self.system_token = self.token_provider.create_token(
            self.id_system, self.login_system, False, self.managed_system_user.authorities
        )
        self.system_headers = {
            TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + self.system_token
        }

    def tearDown(self):
        super().tearDown()

    def __create_admin_user(self) -> User:
        cpy = deepcopy(self.user)
        cpy.id = None
        cpy.authorities = [AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN]
        return cpy.save()

    def __get_headers_for_user_by_login(self, login) -> dict:
        u: User = User.objects(login=login).first()
        token = self.token_provider.create_token(str(u.id), login, False, u.authorities)
        return {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

    # ######### #
    # POST test #
    # ######### #

    def test_CreateWithUnauthenticated(self):
        self.check_unauthorized(self.url_prefix, 'post', None, None, None)

    # no data
    def test_CreateUserWithNoDataAndInvalidContentType(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        self.check_unsupported_media_type(self.url_prefix, 'post', None, headers, None)

    def test_CreateUserWithNoData(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)
        headers.update({'Content-Type': 'application/json'})
        self.check_bad_request(self.url_prefix, 'post', None, headers=headers, query_string=None)

    # empty data
    def test_CreateUserWithEmptyData(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        self.check_bad_request(self.url_prefix, 'post', {}, headers, None)

    # invalid data
    def test_CreateUserWithNoRequiredData(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        data = self.user_vm_json.copy()
        data.pop('login')
        result = self.check_bad_request(self.url_prefix, 'post', data, headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('login' in result.json['detail'])  # required field on detail

    # invalid data
    def test_CreateUserWithUnexpectedField(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        data = self.user_vm_json.copy()
        data['year'] = 2000
        result = self.check_bad_request(self.url_prefix, 'post', data, headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('year' in result.json['detail'])  # invalid field on detail

    # data
    def test_CreateUser(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        timestamp = datetime.now(timezone.utc).replace(tzinfo=None)
        data = self.user_vm_json.copy()
        data['login'] = 'test-' + created.login
        data['email'] = 'test-' + created.email
        result = self.testapp.post(self.url_prefix, json=data, headers=headers, query_string=None)
        self.assertEqual(result.status_code, 201)

        # check on database that has the new user
        user: User = User.objects(login=data['login']).first()
        entity = user.transform()
        self.assertIsNotNone(user)

        for k, v in data.items():
            # convert from json name to attribute name convention
            self.assertIn(k, entity)
            if k == 'password':
                self.assertNotEqual(v, entity[k])  # check password is not save on plain text
            else:
                self.assertEqual(v, entity[k])

        # check that resetKey was created
        self.assertNotEqual(entity['resetKey'], '')

        self.assertGreaterEqual(user.createdDate, timestamp)
        self.assertLessEqual(user.createdDate, datetime.now(timezone.utc).replace(tzinfo=None))
        self.assertGreaterEqual(user.resetDate, timestamp)
        self.assertLessEqual(user.resetDate, datetime.now(timezone.utc).replace(tzinfo=None))

        self.assertEqual(entity['createdBy'], self.security_utils.getCurrentLogin(dict(self.admin_headers)))

    def test_CreateDuplicatedUser(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        data = self.user_vm_json.copy()
        data['login'] = 'test-' + created.login
        data['email'] = 'test-' + created.email
        result = self.testapp.post(self.url_prefix, json=data, headers=headers, query_string=None)
        self.assertEqual(result.status_code, 201)

        # try to create the user again
        self.check_bad_request(self.url_prefix, 'post', data, headers, None)

    def test_CreateUserWithMoreAuthorities(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        timestamp = datetime.now(timezone.utc).replace(tzinfo=None)
        data = self.user_vm_json.copy()
        data['login'] = 'test-' + created.login
        data['email'] = 'test-' + created.email
        data['authorities'] = [AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN]
        result = self.testapp.post(self.url_prefix, json=data, headers=headers, query_string=None)
        self.assertEqual(result.status_code, 201)

        # check on database that has the new user
        user = User.objects(login=data['login']).first()
        self.assertIsNotNone(user)
        entity = user.transform()

        for k, v in data.items():
            self.assertIn(k, entity)
            if k == 'password':
                self.assertNotEqual(v, entity[k])  # check password is not save on plain text
            elif k == 'authorities':
                self.assertEqual([AuthoritiesConstants.USER], entity[k])
            else:
                self.assertEqual(v, entity[k])

        # check that resetKey was created
        self.assertNotEqual(entity['resetKey'], '')

        self.assertGreaterEqual(user.createdDate, timestamp)
        self.assertLessEqual(user.createdDate, datetime.now(timezone.utc).replace(tzinfo=None))
        self.assertGreaterEqual(user.resetDate, timestamp)
        self.assertLessEqual(user.resetDate, datetime.now(timezone.utc).replace(tzinfo=None))

        self.assertEqual(entity['createdBy'], self.security_utils.getCurrentLogin(dict(self.admin_headers)))

    # ######## #
    # PUT test #
    # ######## #

    def test_UpdateWithUnauthenticated(self):
        self.check_unauthorized(self.url_prefix, 'put', None, None, None)

    def test_UpdateUserWithNoDataAndInvalidContentType(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        self.check_unsupported_media_type(self.url_prefix, 'put', None, headers, None)

    def test_UpdateUserWithNoData(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        headers.update({'Content-Type': 'application/json'})
        self.check_bad_request(self.url_prefix, 'put', None, headers=headers, query_string=None)

    # empty data
    def test_UpdateUserWithEmptyData(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        self.check_bad_request(self.url_prefix, 'put', {}, headers, None)

    # invalid data
    def test_UpdateUserWithNoRequiredData(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        data = self.user_vm_json.copy()
        data.pop('login')
        result = self.check_bad_request(self.url_prefix, 'put', data, headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('login' in result.json['detail'])  # required field on detail
        self.assertTrue('id' in result.json['detail'])  # required field on detail

    # invalid data
    def test_UpdateUserWithUnexpectedField(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        data = self.user_vm_json.copy()
        data['id'] = 2000
        data['year'] = 2000
        result = self.check_bad_request(self.url_prefix, 'put', data, headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('year' in result.json['detail'])  # invalid field on detail

    # update and non existing user
    def test_UpdateNonExistingUser(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        data = self.user_vm_json.copy()
        data['id'] = '54759eb3c090d83494e2d804'
        data['login'] = 'test-' + created.login
        data['email'] = 'test-' + created.email
        self.check_not_found(self.url_prefix, 'put', data, headers, None)

    def test_UpdateAnExistingUser(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        data = self.user_vm_json.copy()
        data['id'] = str(created.id)
        data['login'] += data['login']
        self.check_204(self.url_prefix, 'put', data, headers, None)

        after_update = User.objects(email=created.email).first()
        self.assertEqual(created.id, after_update.id)
        self.assertEqual(after_update.login, self.user_vm_json['login']*2)

        self.assertNotEqual(created.lastModifiedDate, after_update.lastModifiedDate)

    def test_UpdateUserWithLoginAlreadyUsed(self):
        created1 = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created1.login)

        tmp = deepcopy(created1)
        tmp.id = None
        tmp.email = tmp.login + tmp.email
        tmp.login += tmp.login
        tmp.save()

        data = deepcopy(created1)
        data.login = tmp.login
        data.createdDate = None
        data = data.transform()
        result = self.check_bad_request(self.url_prefix, 'put', json=data, headers=headers, query_string=None)
        self.assertIn('login', result.json['detail'])

    def test_UpdateUserWithEmailAlreadyUsed(self):
        created1 = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created1.login)

        tmp = deepcopy(created1)
        tmp.id = None
        tmp.email = tmp.login + tmp.email
        tmp.login += tmp.login
        tmp.save()

        data = deepcopy(created1)
        data.email = tmp.email
        data.createdDate = None
        data = data.transform()
        result = self.check_bad_request(self.url_prefix, 'put', json=data, headers=headers, query_string=None)
        self.assertIn('email', result.json['detail'])

    # ########### #
    # DELETE test #
    # ########### #

    def test_DeleteWithUnauthenticated(self):
        self.check_unauthorized(self.url_prefix, 'delete', None, None, None)

    # no id on path
    def test_DeleteWithoutIdentifier(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        self.check_method_not_allowed(self.url_prefix, 'delete', None, headers, None)

    # Delete non exist entity
    def test_DeleteUserNonExistEntity(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        identifier = '000000000000000000000000'
        path = self.url_prefix + '/' + identifier
        self.check_not_found(path, 'delete', None, headers, None)

    # data
    def test_DeleteUser(self):
        created = self.__create_admin_user()
        headers = self.__get_headers_for_user_by_login(created.login)

        path = self.url_prefix + '/' + created.login

        # send a delete request
        self.check_204(path, 'delete', json=None, headers=headers, query_string=None)

        after = User.objects(login=created.login).first()
        self.assertIsNone(after)

    # ############################### #
    # GET ALL test (standard request) #
    # ############################### #

    def test_GetUserWithUnauthenticated(self):
        self.check_unauthorized(self.url_prefix, 'get', None, None, None)

    # Get non exist entity
    def test_GetUserWithNoDataSaved(self):
        result = self.testapp.get(self.url_prefix, headers=self.admin_headers)

        self.assertEqual(result.status_code, 200)
        self.assertEqual(len(result.json), 0)  # check no data on response

    # Get when storage has less document that the page size
    def test_GetUserWitDataSaved(self):
        created = []
        for cnt in range(PageConfiguration.DEFAULT_SIZE // 2):
            tmp = deepcopy(self.user)
            tmp.login = 'login{}'.format(cnt)
            tmp.email = '{}@example.com'.format(tmp.login)
            created.append(tmp.save())

        # send a get request
        result = self.testapp.get(self.url_prefix, headers=self.admin_headers)

        self.assertEqual(result.status_code, 200)
        self.assertEqual(len(result.json), len(created))  # check that has the right size

        result_ids = list(map(lambda x: x['id'], result.json))
        created_ids = list(map(lambda x: str(x.id), created))
        self.assertListEqual(created_ids, result_ids)  # check data has the ids

    def test_GetUserWitDataSaved2(self):
        created = []
        for cnt in range(PageConfiguration.DEFAULT_SIZE + 1):
            tmp = deepcopy(self.user)
            tmp.login = 'login{}'.format(cnt)
            tmp.email = '{}@example.com'.format(tmp.login)
            created.append(tmp.save())

        # send a get request
        result = self.testapp.get(self.url_prefix, headers=self.admin_headers)

        self.assertEqual(result.status_code, 200)
        self.assertNotEqual(len(result.json), len(created))
        self.assertEqual(len(result.json), PageConfiguration.DEFAULT_SIZE)  # check that has the right size

        result_ids = list(map(lambda x: x['id'], result.json))

        # default get order by id so i can order the ids and delete the last one
        created_ids = list(map(lambda x: str(x.id), created))
        created_ids.sort()
        self.assertListEqual(created_ids[:-1], result_ids)  # check data has the ids

        # check links header

        # link per line
        tmp = result.headers['Link'].split(',')
        # filter link per line
        tmp = [item.replace('>', '').replace('<', '').replace('\"', '').replace(';', '').replace('rel=', '') for
               item in tmp]

        links = {}
        for item in tmp:
            # remove http://localhost
            aux = item.split(' ')
            links[aux[1]] = aux[0].replace('http://localhost', '')

        self.assertNotIn('prev', links)
        self.assertEqual(links['last'], links['next'])
        self.assertNotEqual(links['first'], links['last'])

        # retrieve the next page
        result = self.testapp.get(links['next'], headers=self.admin_headers)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(len(result.json), 1)
        result_ids = list(map(lambda x: x['id'], result.json))
        self.assertListEqual(created_ids[-1:], result_ids)  # check data has the ids

    # ######## #
    # GET test #
    # ######## #

    # Get non exist entity
    def test_GetNonExistEntity(self):
        identifier = '000000000000000000000000'
        path = self.url_prefix + '/' + identifier
        result = self.check_not_found(path, 'get', None, self.admin_headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue(identifier in result.json['detail'])  # not found entity on detail

    # data
    def test_Get(self):
        timestamp = datetime.now(timezone.utc).replace(tzinfo=None)
        created = self.user_service.create_user(ManagedUserVM(**self.user_vm_json), dict(self.admin_headers), True)
        path = self.url_prefix + '/' + created.login

        result = self.testapp.get(path, headers=self.admin_headers)
        self.assertEqual(result.status_code, 200)

        user: User = User.objects(login=created.login).first()
        self.assertIsNotNone(user)
        entity = user.transform()

        for k, v in result.json.items():
            if k not in ['createdDate', 'resetDate', 'lastModifiedDate', 'password']:
                self.assertIn(k, entity)
                self.assertEqual(v, entity[k])

        self.assertGreaterEqual(user.createdDate, timestamp)
        self.assertLessEqual(user.createdDate, datetime.now(timezone.utc).replace(tzinfo=None))
        self.assertGreaterEqual(user.resetDate, timestamp)
        self.assertLessEqual(user.resetDate, datetime.now(timezone.utc).replace(tzinfo=None))
        self.assertGreaterEqual(user.createdDate, timestamp)
        self.assertLessEqual(user.createdDate, datetime.now(timezone.utc).replace(tzinfo=None))
        self.assertGreaterEqual(user.lastModifiedDate, timestamp)
        self.assertLessEqual(user.lastModifiedDate, datetime.now(timezone.utc).replace(tzinfo=None))

    # ########### #
    # GET by list #
    # ########### #

    def test_GetLoginWithUnauthenticated(self):
        path = self.url_prefix + '/login'
        self.check_unauthorized(path, 'get', None, None, None)

    def test_GetLoginByIdsWithoutAnyUser(self):
        path = self.url_prefix + '/login/54759eb3c090d83494e2d804,54759eb3c090d83494e2d805'
        result = self.testapp.get(path, headers=self.admin_headers)

        self.assertEqual(result.status_code, 200)
        self.assertListEqual(result.json, [])

    def test_GetLoginByIds(self):
        created = []
        data = self.user_vm_json.copy()
        for cnt in range(PageConfiguration.DEFAULT_SIZE):
            data['login'] = 'login{}'.format(cnt)
            data['email'] = '{}@example.com'.format(data['login'])
            tmp = self.user_service.create_user(ManagedUserVM(**data), dict(self.admin_headers), True)
            created.append(tmp)

        ids = list(map(lambda x: str(x.id), created))

        path = self.url_prefix + '/login/' + ','.join(ids)
        result = self.testapp.get(path, headers=self.admin_headers)

        self.assertEqual(result.status_code, 200)
        self.assertEqual(len(result.json), len(created))

        for item in result.json:
            self.assertIn(item['id'], ids)
            self.assertEqual(len([x for x in created if x.login == item['login']]), 1)

    # ############### #
    # GET Authorities #
    # ############### #

    def test_GetLoginWithUnauthenticated2(self):
        path = self.url_prefix + '/authorities'
        self.check_unauthorized(path, 'get', None, None, None)

    def test_GetLoginWithUnauthenticated3(self):
        # insert authorities
        for item in AuthoritiesConstants.all():
            try:
                tmp = Authority(name=item)
                tmp.save()
            except Exception:
                pass

        path = self.url_prefix + '/authorities'
        result = self.testapp.get(path, headers=self.admin_headers)
        self.assertEqual(result.status_code, 200)

        self.assertListEqual(result.json, AuthoritiesConstants.all())


if __name__ == '__main__':    # pragma: no cover
    unittest.main()
