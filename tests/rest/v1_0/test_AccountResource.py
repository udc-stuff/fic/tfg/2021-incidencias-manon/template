import unittest
from datetime import datetime, timezone

from app.domain.User import User
from app.security.RandomUtil import RandomUtil
from app.security.SecurityUtils import SecurityUtils
from app.security.jwt.TokenProvider import TokenProvider
from app.services.UserService import UserService
from tests.BaseTest import BaseTest
from app.security.AuthoritiesConstants import AuthoritiesConstants


class test_AccountResource(BaseTest):

    user_service = UserService()
    security_utils = SecurityUtils()

    def setUp(self):
        super().setUp()
        self.url_prefix += 'v1.0/'

        self.user_vm_json: dict = {
            'login': 'loginname',
            'email': 'loginname@example.org',
            'password': 's3cr3t',
            'langKey': 'gl_ES'
        }
        self.user: User = User(**self.user_vm_json)

        self.token_provider = TokenProvider()
        self.admin_token = self.token_provider.create_token('0'*24, 'admin', False,
                                                            [AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN])
        self.admin_headers = {
            TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + self.admin_token
        }

    def tearDown(self):
        super().tearDown()

    # ############ #
    # POST account #
    # ############ #

    # no data
    def test_RegisterWithNoDataAndInvalidContentType(self):
        path = self.url_prefix + 'register'
        self.check_unsupported_media_type(path, 'post', None, None, None)

    def test_RegisterWithNoData(self):
        path = self.url_prefix + 'register'
        headers = {
            'Content-Type': 'application/json'
        }
        self.check_bad_request(path, 'post', None, headers=headers, query_string=None)

    # empty data
    def test_RegisterWithEmptyData(self):
        path = self.url_prefix + 'register'
        self.check_bad_request(path, 'post', {}, None, None)

    # invalid data
    def test_RegisterWithNoRequiredData(self):
        path = self.url_prefix + 'register'
        data = self.user_vm_json.copy()
        data.pop('login')
        result = self.check_bad_request(path, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('login' in result.json['detail'])  # required field on detail

    # invalid data
    def test_RegisterWithUnexpectedField(self):
        path = self.url_prefix + 'register'
        data = self.user_vm_json.copy()
        data['year'] = 2000
        result = self.check_bad_request(path, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('year' in result.json['detail'])  # invalid field on detail

    # data
    def test_RegisterUser(self):
        timestamp = datetime.now(timezone.utc).replace(tzinfo=None)
        path = self.url_prefix + 'register'
        data = self.user_vm_json.copy()

        result = self.testapp.post(path, json=data)
        self.assertEqual(result.status_code, 201)

        # check on database that has the new user
        user: User = User.objects(login=data['login']).first()
        self.assertIsNotNone(user)
        entity = user.transform()

        for k, v in data.items():
            # convert from json name to attribute name convention
            self.assertIn(k, entity)
            if k == 'password':
                self.assertNotEqual(v, entity[k])  # check password is not save on plain text
            else:
                self.assertEqual(v, entity[k])

        self.assertNotIn('resetDate', entity)
        self.assertNotIn('resetKey', entity)

        self.assertGreaterEqual(user.createdDate, timestamp)
        self.assertLessEqual(user.createdDate, datetime.now(timezone.utc).replace(tzinfo=None))

        self.assertEqual(entity['createdBy'], '')

    def test_RegisterDuplicatedUser(self):
        path = self.url_prefix + 'register'
        data = self.user_vm_json.copy()
        result = self.testapp.post(path, json=data)
        self.assertEqual(result.status_code, 201)

        # try to create the user again
        self.check_bad_request(path, 'post', data, None, None)

    def test_RegisterWithMoreAuthorities(self):
        path = self.url_prefix + 'register'
        timestamp = datetime.now(timezone.utc).replace(tzinfo=None)
        data = self.user_vm_json.copy()
        data['authorities'] = [AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN
                               ]
        result = self.testapp.post(path, json=data)
        self.assertEqual(result.status_code, 201)

        # check on database that has the new user
        user: User = User.objects(login=data['login']).first()
        self.assertIsNotNone(user)
        entity = user.transform()

        for k, v in data.items():
            # convert from json name to attribute name convention
            self.assertIn(k, entity)
            if k == 'password':
                self.assertNotEqual(v, entity[k])  # check password is not save on plain text
            elif k == 'authorities':
                self.assertEqual([AuthoritiesConstants.USER], entity[k])
            else:
                self.assertEqual(v, entity[k])

        # check that resetKey was created
        self.assertNotIn('resetKey', entity)
        self.assertNotIn('resetDate', entity)

        # response generated after the test
        self.assertGreaterEqual(user.createdDate, timestamp)
        # response generated before this assert
        self.assertLessEqual(user.createdDate, datetime.now(timezone.utc).replace(tzinfo=None))

        self.assertEqual(entity['createdBy'], '')

    # ############ #
    # GET activate #
    # ############ #

    # no data
    def test_ActivateWithoutArgument(self):
        path = self.url_prefix + 'activate'
        result = self.check_bad_request(path, 'get', None, None, None)

        self.assertIn('key', result.json['detail'])

    def test_ActivateWithInvalidArgument(self):
        path = self.url_prefix + 'activate?activation-key=1234'
        result = self.check_bad_request(path, 'get', None, None, None)

        self.assertIn('key', result.json['detail'])

    def test_ActivateWithoutValue(self):
        path = self.url_prefix + 'activate?key'
        self.check_bad_request(path, 'get', None, None, None)

    def test_ActivateWithBadValue(self):
        path = self.url_prefix + 'activate?key=123'
        result = self.check_not_found(path, 'get', None, None, None)

        self.assertIn('key', result.json['title'])

    def test_Activate(self):
        self.user.activationKey = '1234567890'
        self.user.activated = False
        created = self.user.save()

        self.assertFalse(created.activated)

        path = self.url_prefix + 'activate'
        result = self.testapp.get(path, query_string={'key': self.user.activationKey})
        self.assertTrue(result.status_code, 204)

        after_update = User.objects(id=created.id).first()
        self.assertTrue(after_update.activated)
        self.assertEqual(after_update.activationKey, '')

    # ################### #
    # GET is authenticate #
    # ################### #

    def test_IsAuthenticateWithUnauthenticated(self):
        path = self.url_prefix + 'authenticate'
        result = self.testapp.get(path)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.data, b'')

    def test_IsAuthenticateWithAuthenticated(self):
        path = self.url_prefix + 'authenticate'
        result = self.testapp.get(path, headers=self.admin_headers)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.data, b'admin')

    def test_IsAuthenticatedWithExpiredToken(self):
        value = self.token_provider._TokenProvider__token_validity_in_seconds_for_remember_me
        self.token_provider._TokenProvider__token_validity_in_seconds_for_remember_me = -60  # modify private field
        token = self.token_provider.create_token('id-user', 'user', True, [AuthoritiesConstants.USER])
        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}
        self.token_provider._TokenProvider__token_validity_in_seconds_for_remember_me = value  # must be restore

        path = self.url_prefix + 'authenticate'
        result = self.testapp.get(path, headers=headers)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.data, b'')

    # #################### #
    # POST change password #
    # #################### #

    def test_ChangePasswordWithUnauthenticated(self):
        path = self.url_prefix + 'account/change-password'
        self.check_unauthorized(path, 'post', None, None, None)

    # no data
    def test_ChangePasswordWithNoDataAndInvalidContentType(self):
        path = self.url_prefix + 'account/change-password'
        self.check_unsupported_media_type(path, 'post', None, self.admin_headers, None)

    def test_ChangePasswordWithNoData(self):
        path = self.url_prefix + 'account/change-password'
        headers = self.admin_headers.copy()
        headers.update({
            'Content-Type': 'application/json'
        })
        self.check_bad_request(path, 'post', None, headers=headers, query_string=None)

    # empty data
    def test_ChangePasswordWithEmptyData(self):
        path = self.url_prefix + 'account/change-password'
        self.check_bad_request(path, 'post', {}, self.admin_headers, None)

    # invalid data
    def test_ChangePasswordWithNoRequiredData(self):
        path = self.url_prefix + 'account/change-password'
        data = {'currentPassword': 'a'}
        result = self.check_bad_request(path, 'post', data, self.admin_headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('newPassword' in result.json['detail'])  # required field on detail

    # invalid data
    def test_ChangePasswordWithUnexpectedField(self):
        path = self.url_prefix + 'account/change-password'
        data = {'currentPassword': 'a', 'newPassword': 'b', 'year': 2000}
        result = self.check_bad_request(path, 'post', data, self.admin_headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('year' in result.json['detail'])  # invalid field on detail

    def test_ChangePasswordWithShortOne(self):
        path = self.url_prefix + 'account/change-password'
        data = {'currentPassword': 'a', 'newPassword': ''}
        result = self.check_bad_request(path, 'post', data, self.admin_headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('length' in result.json['detail'])  # invalid field on detail

    def test_ChangePasswordWithLongOne(self):
        path = self.url_prefix + 'account/change-password'
        data = {'currentPassword': 'a', 'newPassword': '1234567890'*100}
        result = self.check_bad_request(path, 'post', data, self.admin_headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('length' in result.json['detail'])  # invalid field on detail

    def test_ChangePasswordWithInvalidCurrent(self):
        path = self.url_prefix + 'account/change-password'

        # create a custom user
        self.user.password = '1234567890'
        self.user.activated = False
        self.user.langKey = 'gl-ES'
        self.user.authorities = [AuthoritiesConstants.USER]

        created = self.user.save()
        self.assertFalse(created.activated)

        token = self.token_provider.create_token(str(created.id), created.login, False, created.authorities)
        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        # try to change password
        vm = {'currentPassword': self.user.password, 'newPassword': self.user.password}
        result = self.check_bad_request(path, 'post', vm, headers, None)
        self.assertIn('detail', result.json)
        self.assertEqual(result.json['detail'], 'the current password does not match')

    def test_ChangePassword(self):
        path = self.url_prefix + 'account/change-password'

        # register an user
        password = self.user.password
        self.user.set_password(password)
        self.user.activated = True
        self.user.authorities = [AuthoritiesConstants.USER]
        created = self.user.save()

        token = self.token_provider.create_token(str(created.id), created.login, False, created.authorities)
        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        vm = {'currentPassword': password, 'newPassword': password*2}
        self.check_204(path, 'post', vm, headers, None)

        # can do the same operation because the password change
        self.check_bad_request(path, 'post', vm, headers, None)

        # update the password again
        vm = {'newPassword': password, 'currentPassword': password * 2}
        self.check_204(path, 'post', vm, headers, None)

    # ########### #
    # GET account #
    # ########### #

    def test_GetAccountWithUnauthenticated(self):
        path = self.url_prefix + 'account'
        self.check_unauthorized(path, 'get', None, None, None)

    def test_GetAccount(self):
        path = self.url_prefix + 'account'

        # register an user
        self.user.authorities = [AuthoritiesConstants.USER]
        self.user.activated = True
        created = self.user.save()

        token = self.token_provider.create_token(str(created.id), created.login, False, created.authorities)
        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        result = self.testapp.get(path, headers=headers)
        self.assertEqual(result.status_code, 200)

        self.assertNotIn('activationKey', result.json)
        self.assertNotIn('resetKey', result.json)

        self.assertEqual(result.json['password'], '')
        self.assertEqual(result.json['login'], created.login)
        self.assertEqual(result.json['email'], created.email)

    # ################### #
    # POST reset password #
    # ################### #

    def test_ResetPasswordWithNoData(self):
        path = self.url_prefix + 'account/reset-password/init'
        self.check_bad_request(path, 'post', None, None, None)

    # no user
    def test_ResetPasswordWithNoUser(self):
        path = self.url_prefix + 'account/reset-password/init'
        self.check_not_found(path, 'post', 'Invalid', None, None)

    def test_ResetPassword(self):
        timestamp = datetime.now(timezone.utc).replace(tzinfo=None)
        path = self.url_prefix + 'account/reset-password/init'

        self.user.authorities = [AuthoritiesConstants.USER]
        self.user.activated = True
        created = self.user.save()

        result = self.testapp.post(path, data=created.email)
        self.assertEqual(result.status_code, 204)

        user: User = User.objects(id=created.id).first()
        self.assertIsNotNone(user)
        after_update = user.transform()

        for k, v in created.transform().items():
            if k in ['resetKey', 'resetDate', 'createdDate']:
                continue
            self.assertEqual(v, after_update[k])

        self.assertIsNotNone(after_update['resetKey'])
        self.assertNotEqual(after_update['resetKey'], '')

        self.assertIsNotNone(after_update['resetDate'])
        self.assertGreaterEqual(user.resetDate, timestamp)
        self.assertLessEqual(user.resetDate, datetime.now(timezone.utc).replace(tzinfo=None))

    # ########################## #
    # POST finish reset password #
    # ########################## #

    # no data
    def test_FinishResetPasswordWithNoDataAndInvalidContentType(self):
        path = self.url_prefix + 'account/reset-password/finish'
        self.check_unsupported_media_type(path, 'post', None, None, None)

    def test_FinishResetPasswordWithNoData(self):
        path = self.url_prefix + 'account/reset-password/finish'
        headers = {'Content-Type': 'application/json'}
        self.check_bad_request(path, 'post', None, headers, None)

    # empty data
    def test_FinishResetPasswordWithEmptyData(self):
        path = self.url_prefix + 'account/reset-password/finish'
        self.check_bad_request(path, 'post', {}, None, None)

    # invalid data
    def test_FinishResetPasswordWithNoRequiredData(self):
        path = self.url_prefix + 'account/reset-password/finish'
        data = {'key': 'a'}
        result = self.check_bad_request(path, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('newPassword' in result.json['detail'])  # required field on detail

    # invalid data
    def test_FinishResetPasswordWithUnexpectedField(self):
        path = self.url_prefix + 'account/reset-password/finish'
        data = {'key': 'a', 'newPassword': 'b', 'year': 2000}
        result = self.check_bad_request(path, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('year' in result.json['detail'])  # invalid field on detail

    def test_FinishResetPasswordWithShortOne(self):
        path = self.url_prefix + 'account/reset-password/finish'
        data = {'key': 'a', 'newPassword': 'b'}
        result = self.check_bad_request(path, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('length' in result.json['detail'])  # invalid field on detail

    def test_FinishResetPasswordWithLongOne(self):
        path = self.url_prefix + 'account/reset-password/finish'
        data = {'key': 'a', 'newPassword': '1234567890'*100}
        result = self.check_bad_request(path, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('length' in result.json['detail'])  # invalid field on detail

    def test_FinishResetPasswordWithNotFoundResetKey(self):
        path = self.url_prefix + 'account/reset-password/finish'
        data = {'key': '0000', 'newPassword': '1234567890'*100}
        result = self.check_bad_request(path, 'post', data, None, None)

        self.assertIn('detail', result.json)
        self.assertTrue('length' in result.json['detail'])  # invalid field on detail

    def test_FinishResetPassword(self):
        path = self.url_prefix + 'account/reset-password/finish'

        # register an user
        password = self.user.password
        self.user.activated = True
        self.user.set_password(password)
        self.user.authorities = [AuthoritiesConstants.USER]
        self.user.resetKey = RandomUtil.generate_reset_key()
        created = self.user.save()

        result = self.testapp.post(path, json={'key': self.user.resetKey, 'newPassword': password * 2})
        self.assertEqual(result.status_code, 204)

        after_update = User.objects(id=created.id).first().transform()

        for k, v in created.transform().items():
            if k in ['resetKey', 'resetDate', 'createdDate']:
                continue
            if k == 'password':
                self.assertNotEqual(v, after_update[k])
            else:
                self.assertEqual(v, after_update[k])

    # ################# #
    # POST save account #
    # ################# #

    def test_SaveAccountWithUnauthenticated(self):
        path = self.url_prefix + 'account'
        self.check_unauthorized(path, 'post', None, None, None)

    # no data
    def test_SaveAccountWithNoDataAndInvalidContentType(self):
        path = self.url_prefix + 'account'
        self.check_unsupported_media_type(path, 'post', None, self.admin_headers, None)

    def test_SaveAccountWithNoData(self):
        path = self.url_prefix + 'account'
        headers = self.admin_headers.copy()
        headers.update({
            'Content-Type': 'application/json'
        })
        self.check_bad_request(path, 'post', None, headers, None)

    # empty data
    def test_SaveAccountWithEmptyData(self):
        # register an user
        self.user.activated = True
        self.user.authorities = [AuthoritiesConstants.USER]
        self.user.createdDate = datetime.now(timezone.utc)
        created = self.user.save()

        token = self.token_provider.create_token(str(created.id), created.login, False, created.authorities)
        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        path = self.url_prefix + 'account'
        self.check_204(path, 'post', {}, headers, None)

    # invalid data
    def test_SaveAccountWithUnexpectedField(self):
        path = self.url_prefix + 'account'
        data = {'year': 2000}
        result = self.check_bad_request(path, 'post', data, self.admin_headers, None)

        self.assertIn('detail', result.json)
        self.assertTrue('year' in result.json['detail'])  # invalid field on detail

    # test standard user
    def test_SaveAccountWithRoleUser(self):
        path = self.url_prefix + 'account'

        # register an user
        self.user.activated = True
        self.user.authorities = [AuthoritiesConstants.USER]
        self.user.createdDate = datetime.now(timezone.utc)
        created = self.user.save()

        token = self.token_provider.create_token(str(created.id), created.login, False, created.authorities)
        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        info = {'firstName': 'name', 'lastName': 'surname', 'langKey': 'ru_RU', 'email': 'email@example.org'}

        result = self.testapp.post(path, json=info, headers=headers)
        self.assertEqual(result.status_code, 204)

        after_update = User.objects(id=created.id).first().transform()

        for k, v in info.items():
            self.assertEqual(v, after_update[k])

        # non admin users cannot differ their login and email
        self.assertEqual(info['email'], after_update['login'])

    # test standard user
    def test_SaveAccountWithRoleAdmin(self):
        path = self.url_prefix + 'account'

        # register an user
        self.user.activated = True
        self.user.authorities = [AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN]
        self.user.createdDate = datetime.now(timezone.utc)
        created = self.user.save()

        token = self.token_provider.create_token(str(created.id), created.login, False, created.authorities)
        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        info = {'firstName': 'name', 'lastName': 'surname', 'langKey': 'ru_RU', 'email': 'email@example.org'}

        result = self.testapp.post(path, json=info, headers=headers)
        self.assertEqual(result.status_code, 204)

        after_update = User.objects(id=created.id).first().transform()

        for k, v in info.items():
            self.assertEqual(v, after_update[k])

        # admin can have different values on login and email
        self.assertNotEqual(info['email'], after_update['login'])


if __name__ == '__main__':    # pragma: no cover
    unittest.main()
