import unittest

from tests.BaseTest import BaseTest


class test_Heartbeat(BaseTest):

    def setUp(self):
        super().setUp()
        self.url_prefix += 'heartbeat'

    def tearDown(self):
        super().tearDown()

    def test_heartbeat(self):
        self.check_204(self.url_prefix, 'get', None, None, None)

    def test_not_heartbeat(self):
        for item in ['post', 'put', 'delete']:
            self.check_not_found(self.url_prefix, item, None, None, None)


if __name__ == '__main__':    # pragma: no cover
    unittest.main()
