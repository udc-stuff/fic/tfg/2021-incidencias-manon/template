import unittest
import inspect
from flask import g

from app.services.MailService import MailService
from config import BasicConfiguration
from tests.BaseTest import BaseTest


class test_MailService(BaseTest):

    def setUp(self):
        import os
        if os.getcwd().endswith('/tests/services'):
            os.chdir(os.getcwd()[:-len("/test/services")])

        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_sendHtmlMail(self):
        message: str = '<p>Message create on {} for testing purposes</p>' \
                       '<it>Generated on {}:{}</it>'\
            .format(self.__class__.__name__, BasicConfiguration.APP_NAME, BasicConfiguration.VERSION)

        subject: str = 'Testing prove from ({})'.format(inspect.stack()[0][3])
        to: str = "{}@mail.com".format(BasicConfiguration.APP_NAME)
        bcc: str = "no-reply@mail.com"
        reply_to: str = "no-reply@mail.com"

        # with self.app.app_context():
        with self.app.test_request_context():
            g.correlation_id = inspect.stack()[0][3]
            with self.mail.record_messages() as outbox:
                mail_service = MailService()
                mail_service.send_email_html(subject, message, to, bcc, reply_to)

                self.assertEqual(len(outbox), 1, 'Subjects = {}'.format([o.subject for o in outbox]))
                self.assertEqual(outbox[0].subject, subject)

    def test_sendTextMail(self):
        message: str = 'Message create on {} for testing purposes\n\n' \
                       'Generated on {}:{}'\
            .format(self.__class__.__name__, BasicConfiguration.APP_NAME, BasicConfiguration.VERSION)

        subject: str = 'Testing prove from ({})'.format(inspect.stack()[0][3])
        to: str = "{}@mail.com".format(BasicConfiguration.APP_NAME)
        bcc: str = "no-reply@mail.com"
        reply_to: str = "no-reply@mail.com"

        # with self.app.app_context():
        with self.app.test_request_context():
            g.correlation_id = inspect.stack()[0][3]
            with self.mail.record_messages() as outbox:
                mail_service = MailService()
                mail_service.send_email_text(subject, message, to, bcc, reply_to)

                self.assertEqual(len(outbox), 1, 'Subjects = {}'.format([o.subject for o in outbox]))
                self.assertEqual(outbox[0].subject, subject)

    def test_sendMailWithFullLocales(self):
        # with self.app.app_context():
        with self.app.test_request_context():
            g.correlation_id = inspect.stack()[0][3]
            self.__sendMailLocale(['en-UK', 'es-ES'], True)

    def test_sendMailWithHalfLocales(self):
        # with self.app.app_context():
        with self.app.test_request_context():
            g.correlation_id = inspect.stack()[0][3]
            self.__sendMailLocale(['en', 'es'], True)

    def test_sendMailWithUnimplementedLocales(self):
        # with self.app.app_context():
        with self.app.test_request_context():
            g.correlation_id = inspect.stack()[0][3]
            self.__sendMailLocale(['', 'pt-PT', 'gl-ES'], False)

    def __sendMailLocale(self, locales, implemented):
        to: str = "{}@mail.com".format(BasicConfiguration.APP_NAME)
        mail_service = MailService()

        with self.mail.record_messages() as outbox:
            for locale in locales:
                mail_service.send_activation_email(to, to, locale)

            self.assertEqual(len(outbox), len(locales),
                             'Subjects = {}'.format([o.subject for o in outbox]))

            subjects = [o.subject for o in outbox]
            if implemented:
                self.assertEqual(len(locales), len(set(subjects)))  # to check that are unique
            else:
                self.assertEqual(len(set(subjects)), 1)  # to check that all have the same subject

    def send_ActivationMail(self):
        to: str = "{}@mail.com".format(BasicConfiguration.APP_NAME)
        mail_service = MailService()

        with self.mail.record_messages() as outbox:
            mail_service.send_activation_email(to, to, 'en-GB')
            self.assertEqual(len(outbox), 1)

    def send_PasswordResetMail(self):
        to: str = "{}@mail.com".format(BasicConfiguration.APP_NAME)
        mail_service = MailService()

        with self.mail.record_messages() as outbox:
            mail_service.send_password_reset_email(to, to, 'en-GB')
            self.assertEqual(len(outbox), 1)


if __name__ == '__main__':    # pragma: no cover
    unittest.main()
