from datetime import datetime

import jwt
import unittest

from app.security.AuthoritiesConstants import AuthoritiesConstants
from app.security.jwt.TokenProvider import TokenProvider
from tests.BaseTest import BaseTest


class test_TokenProvider(BaseTest):

    ROLE_USER = [AuthoritiesConstants.USER]
    ROLE_ADMIN = [AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN]

    JWT_SECRET: str = "s3cr3t"

    def setUp(self):
        super().setUp()
        self.token_provider = TokenProvider()

    def tearDown(self):
        super().tearDown()

    def test_returnFalseWhenJWTHasInvalidSignature(self):
        is_token_valid: bool = self.token_provider.validate_token(self.__createTokenWithDifferentSignature())
        self.assertFalse(is_token_valid)

    def test_returnFalseWhenJWTisMalformed(self):
        token: str = self.token_provider.create_token('identifier', 'login', False, self.ROLE_USER)
        invalid_token: str = token[:-1]

        is_token_valid: bool = self.token_provider.validate_token(invalid_token)
        self.assertFalse(is_token_valid)

    def test_returnFalseWhenJWTisExpired(self):
        self.token_provider._TokenProvider__token_validity_in_seconds_for_remember_me = -60  # modify private field
        token: str = self.token_provider.create_token('identifier', 'login', True, self.ROLE_USER)
        self.assertGreater(len(token), 0)

        is_token_valid: bool = self.token_provider.validate_token(token)
        self.assertFalse(is_token_valid)

    def test_returnFalseWhenJWTisUnsupported(self):
        # payload should have some claim, the list is to increase the coverage
        tmp = [None, {'id': 'identifier'}, {'id': 'id', 'sub': 'sub'}]

        for payload in tmp:
            unsupported_token: str = self.__createUnsupportedToken(payload=payload).decode("utf-8")
            is_token_valid: bool = self.token_provider.validate_token(unsupported_token)
            self.assertFalse(is_token_valid)

    def test_returnFalseWhenJWTHeaderIsInvalid(self):
        token: str = self.token_provider.create_token('identifier', 'login', False, self.ROLE_USER)
        is_token_valid: bool = self.token_provider.validate_token(token[10:])
        self.assertFalse(is_token_valid)

    def test_returnFalseWhenJWTSignatureIsInvalid(self):
        token: str = self.token_provider.create_token('identifier', 'login', False, self.ROLE_USER)
        is_token_valid: bool = self.token_provider.validate_token(token[:-10])
        self.assertFalse(is_token_valid)

    def test_returnFalseWhenJWTisInvalid(self):
        token: str = self.token_provider.create_token('identifier', 'login', False, self.ROLE_USER)
        is_token_valid: bool = self.token_provider.validate_token(token[10:-10])
        self.assertFalse(is_token_valid)

    def test_checkToken(self):
        self.__checkToken(False)
        self.__checkToken(True)

        self.__checkToken(False, roles=self.ROLE_ADMIN)
        self.__checkToken(True, roles=self.ROLE_ADMIN)

    def test_returnEmptyWhenEmptyHeader(self):
        headers: dict = {}
        token = self.token_provider.resolve_token(headers)
        self.assertEqual(token, '')

    def test_returnEmptyWhenHeaderWithoutAuthorization(self):
        headers: dict = {'X-test-header': 'test'}
        token = self.token_provider.resolve_token(headers)
        self.assertEqual(token, '')

    def test_returnEmptyWhenInvalidAuthorizationHeader(self):
        token: str = self.token_provider.create_token('identifier', 'login', False, self.ROLE_USER)

        # No bearer but token
        headers: dict = {self.token_provider.AUTHORIZATION_HEADER: token}
        self.assertEqual(self.token_provider.resolve_token(headers), '')

        # bearer and token without space
        headers: dict = {
            self.token_provider.AUTHORIZATION_HEADER:
                self.token_provider.AUTHORIZATION_HEADER_PREFIX.strip() + token
        }
        self.assertEqual(self.token_provider.resolve_token(headers), '')

    def test_returnTokenFromHeaders(self):
        token = self.token_provider.create_token('identifier', 'login', False, self.ROLE_USER)

        headers: dict = {
            self.token_provider.AUTHORIZATION_HEADER:
                '{}{}'.format(self.token_provider.AUTHORIZATION_HEADER_PREFIX, token)
        }

        result = self.token_provider.resolve_token(headers)
        self.assertEqual(token, result)

    def test_validateFakePayloads(self):
        identifier = login = 'user'
        now = datetime.timestamp(datetime.now())
        payload = {
            'iss': TokenProvider.ISSUER,
            'id': identifier,
            'sub': login,
            'exp': now + 60,
            'iat': now,
            'jti': '{}-{}'.format(identifier, now),
            'roles': []
        }

        for item in ['exp', 'sub', 'id']:
            tmp = payload.copy()
            tmp.pop(item)
            token = jwt.encode(tmp, self.JWT_SECRET, TokenProvider._TokenProvider__algorithm).decode('utf8')
            self.assertFalse(self.token_provider.validate_token(token, self.JWT_SECRET))

    def __checkToken(self, remember: bool, roles=None):
        identifier: str = 'identifier'
        login: str = 'login'
        roles = roles or self.ROLE_USER
        token: str = self.token_provider.create_token(identifier, login, remember, roles)

        is_token_valid: bool = self.token_provider.validate_token(token)
        self.assertTrue(is_token_valid)

        self.assertEqual(identifier, self.token_provider.get_id_from_token(token))
        self.assertEqual(login, self.token_provider.get_login_name_from_token(token))
        self.assertListEqual(roles, self.token_provider.get_roles_from_token(token))

    def __createUnsupportedToken(self, payload=None):
        tmp = payload or {'payload': 'payload'}
        return jwt.encode(tmp, self.JWT_SECRET, self.token_provider._TokenProvider__algorithm)

    def __createTokenWithDifferentSignature(self):
        return self.token_provider.create_token('identifier', 'login', False, self.ROLE_USER, key=self.JWT_SECRET*2)


if __name__ == '__main__':    # pragma: no cover
    unittest.main()
