from app.domain.User import User
from app.security.AuthoritiesConstants import AuthoritiesConstants
from app.security.SecurityUtils import SecurityUtils
from app.security.jwt.TokenProvider import TokenProvider
from tests.BaseTest import BaseTest


class test_TokenProvider(BaseTest):

    ROLE_USER: list = [AuthoritiesConstants.USER]
    ROLE_ADMIN: list = ROLE_USER + [AuthoritiesConstants.ADMIN]

    JWT_SECRET: str = "s3cr3t"

    def setUp(self):
        super().setUp()
        self.token_provider = TokenProvider()
        self.security_utils = SecurityUtils()

        self.test_user_id = 'identifier'
        self.test_login = 'login'
        self.test_roles = self.ROLE_USER
        self.test_token = self.token_provider.create_token(self.test_user_id, self.test_login, False, self.test_roles)
        self.test_headers = {
            TokenProvider.AUTHORIZATION_HEADER:
            TokenProvider.AUTHORIZATION_HEADER_PREFIX+self.test_token
        }

    def tearDown(self):
        super().tearDown()

    def __get_header_token_no_prefix(self):
        tmp = self.test_headers.copy()
        tmp[TokenProvider.AUTHORIZATION_HEADER] = \
            tmp[TokenProvider.AUTHORIZATION_HEADER].replace(TokenProvider.AUTHORIZATION_HEADER_PREFIX, '')

        return tmp

    def __get_header_token_with_invalid_hash(self):
        tmp = self.test_headers.copy()
        tmp[TokenProvider.AUTHORIZATION_HEADER] = tmp[TokenProvider.AUTHORIZATION_HEADER].replace('.', ':')

        return tmp

    # getCurrentId
    def test_getCurrentIdWithNoHeader(self):
        result = self.security_utils.getCurrentUserId({})

        self.assertNotEqual(result, self.test_user_id)
        self.assertEqual(result, '')

    def test_getCurrentIdWithNoPrefix(self):
        h = self.__get_header_token_no_prefix()

        result = self.security_utils.getCurrentUserId(h)

        self.assertNotEqual(result, self.test_user_id)
        self.assertEqual(result, '')

    def test_getCurrentIdWithInvalidHash(self):
        h = self.__get_header_token_with_invalid_hash()

        result = self.security_utils.getCurrentUserId(h)

        self.assertNotEqual(result, self.test_user_id)
        self.assertEqual(result, '')

    def test_getCurrentId(self):
        result = self.security_utils.getCurrentUserId(self.test_headers)
        self.assertEqual(result, self.test_user_id)

    # getCurrentLogin
    def test_getCurrentLoginWithNoHeader(self):
        result = self.security_utils.getCurrentLogin({})

        self.assertNotEqual(result, self.test_login)
        self.assertEqual(result, '')

    def test_getCurrentLoginWithNoPrefix(self):
        h = self.__get_header_token_no_prefix()

        result = self.security_utils.getCurrentLogin(h)

        self.assertNotEqual(result, self.test_login)
        self.assertEqual(result, '')

    def test_getCurrentLoginWithInvalidHash(self):
        h = self.__get_header_token_with_invalid_hash()

        result = self.security_utils.getCurrentLogin(h)

        self.assertNotEqual(result, self.test_login)
        self.assertEqual(result, '')

    def test_getCurrentLogin(self):
        result = self.security_utils.getCurrentLogin(self.test_headers)
        self.assertEqual(result, self.test_login)

    # isAuthenticated
    def test_isAuthenticatedWithNoHeader(self):
        self.assertFalse(self.security_utils.isAuthenticated({}))

    def test_isAuthenticatedWithNoPrefix(self):
        h = self.__get_header_token_no_prefix()

        self.assertFalse(self.security_utils.isAuthenticated(h))

    def test_isAuthenticatedWithInvalidHash(self):
        h = self.__get_header_token_with_invalid_hash()

        self.assertFalse(self.security_utils.isAuthenticated(h))

    def test_isAuthenticated(self):
        self.assertTrue(self.security_utils.isAuthenticated(self.test_headers))

    # isCurrentUserInRole
    def test_isAuthenticatedWithNoHeader2(self):
        for role in self.ROLE_ADMIN:
            self.assertFalse(self.security_utils.isCurrentUserInRole({}, role))

    def test_isAuthenticatedWithNoPrefix2(self):
        h = self.__get_header_token_no_prefix()

        for role in self.ROLE_ADMIN:
            self.assertFalse(self.security_utils.isCurrentUserInRole(h, role))

    def test_isAuthenticatedWithInvalidHash2(self):
        h = self.__get_header_token_with_invalid_hash()

        for role in self.ROLE_ADMIN:
            self.assertFalse(self.security_utils.isCurrentUserInRole(h, role))

    def test_isAuthenticated2(self):
        roles = self.test_roles
        created: User = User(login='login', email='login@example.org', activated=True, langKey='en', authorities=roles)
        created = created.save()

        token = self.token_provider.create_token(str(created.id), created.login, False, created.authorities)
        headers = {TokenProvider.AUTHORIZATION_HEADER: TokenProvider.AUTHORIZATION_HEADER_PREFIX + token}

        for role in self.test_roles:
            self.assertTrue(self.security_utils.isCurrentUserInRole(headers, role))

        tmp = [x for x in self.ROLE_ADMIN if x not in self.test_roles]

        for role in tmp:
            self.assertFalse(self.security_utils.isCurrentUserInRole(self.test_headers, role))

    # getHeaderAndPayload
    def test_getHeaderAndPayloadWithNoHeader(self):
        result = self.security_utils.getHeaderAndPayload({})
        self.assertEqual(len(result), 0)

    def test_getHeaderAndPayloadWithNoPrefix(self):
        h = self.__get_header_token_no_prefix()

        result = self.security_utils.getHeaderAndPayload(h)
        self.assertEqual(len(result), 0)

    def test_getHeaderAndPayloadWithInvalidHash(self):
        h = self.__get_header_token_with_invalid_hash()

        result = self.security_utils.getHeaderAndPayload(h)
        self.assertEqual(len(result), 0)

    def test_getHeaderAndPayload(self):
        result = self.security_utils.getHeaderAndPayload(self.test_headers)

        self.assertGreater(len(result), 1)
        self.assertTrue(self.test_token.startswith(result))
