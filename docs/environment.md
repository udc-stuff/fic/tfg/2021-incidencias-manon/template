# Environment

This project can be configured using environment variables. 
This documentation explains the meaning of the environment variables.


## Basic Configuration

- **BASIC_BASE_URL**: url where the service is allocated
- **BASIC_SUPPORT_MAIL**: Mail used to provide support
- **BASIC_LOG_DIR**: Directory where logs are stored
- **BASIC_LOGGER_LEVEL**: (DEBUG | INFO | WARNING | ERROR | CRITICAL)  log level to record the messages


## Mail Configuration

- **MAIL_USERNAME**: username used for the mail connection
- **MAIL_PASSWORD**: password used for the mail connection
- **MAIL_SERVER**: dnsname of the database server
- **MAIL_PORT**: port where database is listening
- **MAIL_USE_SSL**: (True | False) indicate if mail use SSL
- **MAIL_USE_TLS**: (True | False) indicate if mail use TLS

## Security Configuration

- **JWT_SECRET**: Secret used to generate the signature of the JsonWebToken

## Limit Configuration

- **LIMIT_DEFAULT_REQUEST**: amount of request allowed in a period of time
- **LIMIT_DEFAULT_WINDOW**: time window (in seconds) for the limitation of requests

## DatabaseConfiguration

- **DATABASE_USERNAME**: username used for the connection
- **DATABASE_PASSWORD**: password used for the connection
- **DATABASE_SERVER**: dnsname of the database server
- **DATABASE_PORT**: port where database is listening
- **DATABASE_QUERY_CONFIGURATION**: configuration passed on the query param
- **DATABASE_PROTOCOL**: (mongodb | mongomock)

