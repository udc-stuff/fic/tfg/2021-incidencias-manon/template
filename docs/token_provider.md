# Token provider


## Payload information

### Public claims

| claim | implemented | description |
| :---- | :---------- | :---------- |
| iss | Yes  | The issuer of the token |
| sub | Yes | The subject of the token |
| aud | No  | The audience of the token |
| exp | Yes | This will probably be the registered claim most often used. This will define the expiration in NumericDate value. The expiration MUST be after the current date/time. |
| nbf | No  | Defines the time before which the JWT MUST NOT be accepted for processing |
| iat | Yes | The time the JWT was issued. Can be used to determine the age of the JWT |
| jti | Yes | Unique identifier for the JWT. Can be used to prevent the JWT from being replayed. This is helpful for a one time use token. |

 

### Private claims

These claims are on-premise.

| claim | type | description |
| :---- | :--- | :---------- |
| id    | str  | The identifier of the user. It used to get quickly the user information from the database |
| roles | list of str | The roles of the user. It used on microservices to authorized request without to a request to other users |



### Considerations


#### jti claim
**jti** claim is added on payload but there is not an implementation.
This claim is usually used to revoke authentication. 

An implementation to revoke can be added in the future.
Proposed idea if the implementation is required:

users microservice (or the microservice to generate tokens) should:

- storage jti, iat and exp claims for all generated tokens.
- set a True/False mark to indicate if the token was revoked.
- implement a resource to revoke the tokens
- implement a resource to request for other microservices if the token was revoked.
- Improvement: remove expired tokens from the storage.
- Improvement: use a distributed cache with TTL for all revoked tokens.
- Improvement: use a local cache after asking in a distributed cache.

Other microservices should:

- Check on each request that required authentication if the token was revoked
- Considerations

    - This behaviour will be in most request so will affect other services if the response is slow.
    - The request of this resource should have a short timeout. Should consider the token was not revoked if the timeout is reached.
    - The request of this resource shouldn't have a backoff algorithm. Should consider the token was not revoked if the request can't be processed.
    

The nice point of this implementation is that it doesn't matter the way of jti was generated.
Only was constraint is required: jti must be unique on each token. 
So can be change the way to create a jti claim.
Right now the implementation of jti is simple, just join login, and the time of token was issued.

Maybe it is better to put not an obvious jti claim.
A possible implementation of short and unique jti claim is:

```python
import base64
import hashlib
import uuid

id: str  # It is passed to the method in charge of creating the token
iat: str  # generated when token is processed

unique_id = '{}-{}'.format(id, iat)

hash = hashlib.sha1(str(unique_id).encode("UTF-8"))

# The digest is shorted because use more than just hex characters (sha1 return hexa string)
jti = base64.b64encode(hash.digest())

```


### roles claim

The official JWT site explicitly mentions "authorization" (in contrast to "authentication") as an usecase for JWTs:

> When should you use JSON Web Tokens? **Authorization**: This is the most common scenario for using JWT. Once the user is logged in, each subsequent request will include the JWT, allowing the user to access routes, services, and resources that are permitted with that token. Single Sign On is a feature that widely uses JWT nowadays, because of its small overhead and its ability to be easily used across different domains.

That being said, from a security-perspective you should think twice whether you really want to include roles or permissions in the token.

_(The text below can be understood as a more "in-depth" follow up to the rather short-kept accepted answer)_

Once you created and signed the token you grant the permission until the token expires. But what if you granted admin permissions by accident? Until the token expires, somebody is now operating on your site with permissions that were assigned by mistake.

ome people might argue that the token is short-lived, but this is not a strong argument given the amount of harm a person can do in short time. Some other people advocate to maintain a separate blacklist database table for tokens, which solves the problem of invalidating tokens, but adds some kind of session-state tracking to the backend, because you now need to keep track of all current sessions that are out there – so, you would then have to make a db-call to the blacklist every time a request arrives to make sure it is not blacklisted yet. One may argue that this defeats the purpose of "putting the roles into the JWT to avoid an extra db-call" in the first place, since you just traded the extra "roles db-call" for an extra "blacklist db-call".

So instead of adding authorization claims to the token, you could keep information about user roles and permissions in your auth-server's db over which you have full control at any time (e.g. to revoke a certain permission for a user). If a request arrives, you fetch the current roles from the auth-server (or wherever you store your permissions).

By the way, if you have a look at the list of [public claims registered by the IANA]<https://www.iana.org/assignments/jwt/jwt.xhtml>, you will see that these claims evolve around authentication and are not dealing with what the user is allowed to do (authorization).

So in summary you can...

- **add roles to your JWT** if (a) convenience is important to you and (b) you want to avoid extra database calls to fetch permissions and (c) do not care about small-time windows in which a person has rights assigned he shouldn't have and (d) you do not care about the (slight) increase in the JWT's payload resulting from adding the permissions.
- **add roles to your JWT and use a blacklist** if (a) you want to prevent any time windows in which a person has rights assigned he shouldn't have and (b) accept that this comes at the cost of making a request to a blacklist for every incoming request and (c) you do not care about the (slight) increase in the JWT's payload resulting from adding the permissions.
- **not add roles to your JWT and fetch them on demand** if (a) you want to prevent any time windows in which a person has rights assigned he shouldn't have or (b) avoid the overhead of a blacklist or (c) avoid increasing the size of your JWT payload to increase slightly and (d) if you accept that this comes at the cost of sometimes/always querying the roles on incoming requests.
