# Logger

3 types of logging statement will be emitted by this project:

- Request log: request instrumentation logging statement which recorded request information
- Response log: request instrumentation logging statement which recorded response information
- Application log: normal logging statement


## Common field

| Field | Description | Format | Example |
| :---- | :---------- | :----- | :------ |
| written_at | The date when this log message was written | ISO | 2020-08-29T04:21:27.240402+02:00 |
| correlation_id | Identifier attached to requests and messages that allow reference to a particular transaction or event chain | string | base-16186af9-d113-4c5b-9f51-24160b6ebbd3 |
| request_id | Identifier for every incoming HTTP request | string | base-93e70927-7cff-4259-a283-97e935840ce7 |
| type | Type of logging "log", "response" or "request" | string | |
| component_id |  Uniquely identifies the software component that has processed the current request | string | |
| component_name | A human-friendly name representing the software component | string | base:alpha |
| component_instance | Node that runs the instance | string | m1708.local |
| level | The log "level" indicating the severity of the log message | string | INFO |
| logger | The logger name that emits the log message | string | app name |
| process | Process name (if available) | string | MainProcess |
| thread | Identifies the execution thread in which this log message has been written | string | Thread-15 |
| filename | Full pathname of the source file where the logging call was issued (if available) | string | base/app/services/MockService.py |
| module | Module (name portion of filename) | string | MockService |
| function | Name of function containing the logging call | string | mock |
| line | Source line number where the logging call was issued (if available) | string | 26 |

 
 
 ## Application log
 
| Field | Description | Format | Example |
| :---- | :---------- | :----- | :------ |
| msg | The actual message string passed to the logger | string | This is a log message |
| exc_info | (optional) Traceback information about an exception | string | |




## Request log

| Field | Description | Format | Example |
| :---- | :---------- | :----- | :------ |
| request | request path that has been processed | string | /heartbeat |
| request_received_at | The date when an incoming request was received by the producer | ISO | 2020-08-29T04:21:27.244022+02:00 |
| protocol | Which protocol was used to issue a request to a producer. In most cases, this will be HTTP (including a version specifier), but for outgoing request reported by a producer, it may contain other values. E.g: a database call via JDBC may report, e.g. "JDBC/1.2" | string | HTTP/1.1 |
| method | The corresponding protocol method | string | GET |
| remote_ip | IP address of the consumer (might be a proxy, might be the actual client)  | string | 192.168.0.1 |
| request_size_b | The size in bytes of the requesting entity of "body" (e.g: in case of POST request) | long | 1234 |
| request_content_type | The MIME type associated with the entity of the request if available/specified | string | application/json |
| user_agent | agent that make the request | string | curl/7.64.1 |
| referer | For HTTP requests, identifies the address of the webpage (i.e. the URI or IRI) that linked to the resource being requested. | string | /index.html |
| x_forwarded_for | Comma-separated list of IP addresses, the left-most being the original client, followed by proxy server addresses that forwarded the client request. | string | 192.0.2.60,10.12.9.23 |
| x_real_ip |  | string | 8.8.8.8 |



## Response log

| Field | Description | Format | Example |
| :---- | :---------- | :----- | :------ |
| request | request path that has been processed | string | /heartbeat |
| method | The corresponding protocol method | string | GET |
| response_status | The status code of the response | long | 200 |
| response_sent_at | The date when the response to an incoming request was sent to the consumer | ISO | 2020-08-29T04:21:27.444222+02:00 |
| response_time_ms | How many milliseconds it took the producer to prepare the response | float | 12 |
| response_size_b | The size in bytes of the response entity | long | 1234 |
| response_content_type | The MIME type associated with the entity of the response if available/specified | string | application/json |
| msg | (optional) The actual message string passed to the logger | string | This is a log message |
| exc_info | (optional) Traceback information about an exception | string | |


## Considerations

### Separated request and response log

It would be easier to see how a request was if the request and response log are united, that is, they are not separate logs.
However, the approximation followed separates the request and the response. This is to avoid losing logs.
If a single log line will be generated, the following could happen: if the request can be made, but it is not capable of generating a response, the log will not be saved.
An example of this situation is that the request is received but before returning the response the server stops, in this situation the log will never be saved, and it will not be possible to know if this situation happened.

Why do the request, and the response keep both the request, protocol and method fields?
This is just to make it more human-readable on a manual read (without a log collection app).
This fields can be deleted on response logs.


### Log without request_id and correlation_id 

Some logs do not have a request and correlation identifiers.
This is because it was generated outside a request (reason for no request_id), and it was generated by something that the server does (reason for no correlation_id).
An example is when the server starts and logs the configuration events (no request and no transaction or event chain).

These logs are "application logs" and never can have the type "request" or "response".

### Log without request_id but correlation_id 

Some logs record and correlation identifier but do not have a request identifier.
This is because it was generated outside the request (reason for no request_id), but it is a task that can be executed multiple times and originates events.
An example is a cron task.

A transaction can have only "application logs" (type="log") if never do a request to other service.

If a transaction make a request to other services should have "request" and "response" logs.
