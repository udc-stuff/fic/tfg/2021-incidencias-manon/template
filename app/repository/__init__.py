"""
That's the place where the connection an other db stuff are allocated
"""
from logging import DEBUG, INFO, WARNING

from mongoengine import connect
from pymongo import monitoring

from app.util.JsonLogger import JsonLogger
from config import DatabaseConfiguration


# monitoring
class CommandLogger(monitoring.CommandListener):

    def started(self, event):
        JsonLogger.application(DEBUG, "Command {0.command_name} with request id {0.request_id} "
                                      "started on server {0.connection_id}".format(event))

    def succeeded(self, event):
        JsonLogger.application(DEBUG, "Command {0.command_name} with request id {0.request_id} "
                                      "on server {0.connection_id} succeeded "
                                      "in {0.duration_micros} microseconds".format(event))

    def failed(self, event):
        JsonLogger.application(WARNING, "Command {0.command_name} with request id {0.request_id} "
                                        "on server {0.connection_id} failed in {0.duration_micros} "
                                        "microseconds".format(event))


# on documentation this command appears before the connection
monitoring.register(CommandLogger())

# establish connection
if DatabaseConfiguration.REQUIRED:
    JsonLogger.application(INFO, 'Ready to make a connection with mongo (protocol={} host={}, port={}, conf={})'
                           .format(
        DatabaseConfiguration.PROTOCOL,
        DatabaseConfiguration.SERVER, DatabaseConfiguration.PORT,
        DatabaseConfiguration.QUERY_CONFIGURATION
    )
                           )

    uri = '{}://'.format(DatabaseConfiguration.PROTOCOL)
    if DatabaseConfiguration.USERNAME or DatabaseConfiguration.PASSWORD:
        uri += '{}:{}@'.format(DatabaseConfiguration.USERNAME, DatabaseConfiguration.PASSWORD)
    uri += DatabaseConfiguration.SERVER
    if DatabaseConfiguration.PORT:
        uri += ':{}'.format(DatabaseConfiguration.PORT)
    if DatabaseConfiguration.DATABASE:
        uri += '/{}'.format(DatabaseConfiguration.DATABASE)
    if DatabaseConfiguration.QUERY_CONFIGURATION:
        uri += '{}'.format(DatabaseConfiguration.QUERY_CONFIGURATION)

    connect(host=uri)

# create entities
JsonLogger.application(INFO, 'CREATE ENTITIES = {}'.format(DatabaseConfiguration.CREATE_ENTITIES))

if DatabaseConfiguration.CREATE_ENTITIES:
    from app.domain.User import User
    from app.domain.Authority import Authority
    from app.security.AuthoritiesConstants import AuthoritiesConstants
    from app.security.RandomUtil import RandomUtil

    JsonLogger.application(DEBUG, 'authority count = {}'.format(Authority.objects().count()))
    JsonLogger.application(DEBUG, 'user count = {}'.format(User.objects().count()))
    if Authority.objects().count() < len(AuthoritiesConstants.all()):
        JsonLogger.application(INFO, 'add authorities on database')
        for item in AuthoritiesConstants.all():
            try:
                tmp = Authority(name=item)
                tmp.save()
            except Exception:
                pass

    if User.objects().count() == 0:
        JsonLogger.application(INFO, 'add user, admin and system users on database')

        user: User = User(login='user', email='user@localhost.com', activated=True, langKey='es'
                          , authorities=[AuthoritiesConstants.USER])
        user.set_password('user')
        user.save()

        user: User = User(login='admin', email='admin@localhost.com', activated=True, langKey='es',
                          authorities=[AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER])
        user.set_password('admin')
        user.save()

        user: User = User(login='system', email='system@localhost.com', activated=False, langKey='en',
                          authorities=[AuthoritiesConstants.SYSTEM, AuthoritiesConstants.ADMIN,
                                       AuthoritiesConstants.USER, AuthoritiesConstants.ANONYMOUS])
        user.set_password(RandomUtil.generate_password())
        user.save()
