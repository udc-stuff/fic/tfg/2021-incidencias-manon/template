from config import PageConfiguration


class PageDatabase:
    """
    class to pass get_all to retrieve a set of entities
    """
    page: int
    size: int
    offset: int
    sort_column: str
    sort_order: str

    def __init__(self, page=None, size=None, sort_column=None, sort_order=None):

        if page is None:
            self.page = 0
        else:
            self.page = page if page >= 0 else 0

        if size is None:
            self.size = PageConfiguration.DEFAULT_SIZE
        else:
            self.size = min(max(PageConfiguration.MIN_SIZE, size), PageConfiguration.MAX_SIZE)

        if sort_column is None or sort_column.strip() == '':
            self.sort_column = 'id'
        else:
            self.sort_column = sort_column

        if sort_order is None or sort_order.strip() == '':
            self.sort_order = '+'
        else:
            if sort_order.lower() == 'desc':
                self.sort_order = '-'
            else:
                self.sort_order = '+'

        self.offset = self.page * self.size
