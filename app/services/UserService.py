import traceback
from datetime import datetime, timezone
from logging import DEBUG, INFO
from typing import List

from mongoengine import DoesNotExist, OperationError, LookUpError, InvalidQueryError, NotUniqueError

from app import JsonLogger, BadRequestException
from app.domain.Authority import Authority
from app.domain.User import User
from app.errors.NotFoundException import NotFoundException
from app.errors.UnauthorizedException import UnauthorizedException
from app.repository.BaseRepository import PageDatabase
from app.rest.vm.ManagedUserVm import ManagedUserVM
from app.security.AuthoritiesConstants import AuthoritiesConstants
from app.security.RandomUtil import RandomUtil
from app.security.SecurityUtils import SecurityUtils
from app.util.PageInfo import PageInfo


class UserService:

    security_utils = SecurityUtils()

    DEFAULT_LANGUAGE = 'es'

    """singleton implementation for this class"""
    def __new__(cls, *args, **kwargs):
        try:
            cls.__instance
        except AttributeError:
            cls.__instance = super(UserService, cls).__new__(cls)
        return cls.__instance

    def __remove_sensible_data(self, user: User) -> User:
        user.password = None
        user.activationKey = None
        user.resetKey = None
        return user

    def create_user(self, user_vm: ManagedUserVM, headers: dict, activated: bool) -> User:
        JsonLogger.application(DEBUG, "run create_user on UserService")
        user: User = user_vm.get_user()

        user.set_password(user_vm.password)
        user.authorities = [AuthoritiesConstants.USER]

        login = self.security_utils.getCurrentLogin(headers)
        user.createdBy = login
        user.lastModifiedBy = login
        user.lastModifiedDate = datetime.now(timezone.utc)

        if not activated:
            user.activationKey = RandomUtil.generate_activation_key()
        else:
            user.set_password = RandomUtil.generate_activation_key()
            user.resetKey = RandomUtil.generate_reset_key()
            user.resetDate = datetime.now(timezone.utc)

        if not user_vm.langKey:
            user.langKey(self.DEFAULT_LANGUAGE)

        try:
            user = user.save()
            return self.__remove_sensible_data(user)
        except NotUniqueError:
            JsonLogger.application(INFO, 'Cannot create a user because unique constraint (login={}, email={})'
                                   .format(user.login, user.email))
            raise BadRequestException('Duplicate Key Error')

    def update_user(self, user_vm: ManagedUserVM, headers: dict) -> User:
        JsonLogger.application(DEBUG, "run update_user on UserService")

        user = User.objects(email=user_vm.email).first()
        if user is not None and str(user.id) != user_vm.id:
            raise BadRequestException('Duplicated value', 'email is already used')

        user = User.objects(login=user_vm.login).first()
        if user is not None and str(user.id) != user_vm.id:
            raise BadRequestException('Duplicated value', 'login is already used')

        user: User = User.objects(id=user_vm.id).first()
        if user is None:
            JsonLogger.application(INFO, 'try to update a non existent entity (id: {})'.format(user_vm.id),
                                   traceback.format_exc())
            raise NotFoundException(description='Not found resource with id {}'.format(user_vm.id))

        user.lastModifiedDate = datetime.utcnow()
        user.lastModifiedBy = self.security_utils.getCurrentLogin(headers)

        try:
            user = user_vm.update_user(user)
            return self.__remove_sensible_data(user.save())

        except DoesNotExist:
            JsonLogger.application(INFO, 'try to update a non existent entity (id: {})'.format(user_vm.id),
                                   traceback.format_exc())
            raise NotFoundException(description='Not found resource with id {}'.format(user_vm.id))
        except (OperationError, LookUpError, InvalidQueryError):
            JsonLogger.application(INFO, 'failed on update an entity with id {}'.format(user_vm.id),
                                   traceback.format_exc())
            raise BadRequestException()

    def delete_user(self, login: str) -> User:
        JsonLogger.application(DEBUG, "run delete_user on UserService")

        user: User = User.objects(login=login).first()
        if user is None:
            JsonLogger.application(INFO, 'try to delete a non existent entity (login: {})'.format(login))
            raise NotFoundException(description='No user with login {} to delete'.format(login))

        user.delete()
        JsonLogger.application(INFO, 'deleted user with id={}, login={}'.format(user.id, user.login))

        return user

    def get_all(self, number: int = 0, size: int = 20, sort_column: str = '', sort_order: str = '') -> PageInfo:
        JsonLogger.application(DEBUG, "run get_all on UserService (number:{}, size:{})".format(number, size))

        page: PageDatabase = PageDatabase(number, size, sort_column, sort_order)
        if sort_column is not None and sort_column != '':
            results = User.objects().order_by("{}{}".format(page.sort_order, page.sort_column))\
                .skip(page.offset).limit(page.size)
        else:
            results = User.objects.skip(page.offset).limit(page.size)

        # remove sensible data
        for user in results:
            self.__remove_sensible_data(user)
        total = User.objects.count()

        page_info: PageInfo = PageInfo(page, results, total)

        return page_info

    def get_user(self, login: str):
        JsonLogger.application(DEBUG, "run get on UserService")
        user: User = User.objects(login=login).first()
        if user is None:
            raise NotFoundException(description='No found user with login {}'.format(login))

        # remove sensible data
        return self.__remove_sensible_data(user)

    def get_users_by_ids(self, ids: List[str]) -> List[User]:
        JsonLogger.application(DEBUG, "run get_users_by_ids on UserService")

        if ids is None or len(ids) == 0:
            return []

        tmp = User.objects(id__in=ids)
        return list(map(lambda x: self.__remove_sensible_data(x), tmp))

    def get_authorities(self) -> List[str]:
        JsonLogger.application(DEBUG, "run get_authorities on UserService")

        authorities = Authority.objects()
        tmp: List[str] = list(map(lambda x: x.name, authorities))
        return tmp

    def activate_account(self, key: str) -> User:
        JsonLogger.application(DEBUG, "run activate_account on UserService")

        if key is None or key == '':
            raise BadRequestException('Empty value', 'Not value found on key parameter')

        user: User = User.objects(activationKey=key).first()
        if user is None:
            raise NotFoundException('invalid key')

        user.activationKey = ''
        user.activated = True
        user.save()

        JsonLogger.application(INFO, 'Activated user {}'.format(str(user.id)))
        return user

    def change_password(self, current: str, new: str, headers: dict):
        JsonLogger.application(DEBUG, "run change_password on UserService")

        identifier: str = self.security_utils.getCurrentUserId(headers=headers)
        if not identifier:
            raise UnauthorizedException()

        user: User = User.objects(id=identifier).first()
        if user is None:
            raise NotFoundException()

        aux = User(password=user.password)
        if not aux.check_password(current):
            raise BadRequestException(title='Invalid password', description='the current password does not match')

        user.set_password(new)
        user.save()
        JsonLogger.application(INFO, 'update the password of user {}'.format(user.id))

        return

    def get_user_by_id_without_sensitive_data(self, identifier: str) -> ManagedUserVM:
        JsonLogger.application(DEBUG,
                               'run get_user_by_id_without_sensitive_data on UserService (id={})'.format(identifier))

        user: User = User.objects(id=identifier).first()
        if user is None:
            raise NotFoundException()

        data = ManagedUserVM()
        data.set_user_dto(user)

        return data

    def request_password_reset(self, mail: str) -> User:
        JsonLogger.application(DEBUG, 'run request_password_reset on UserService')
        if mail is None or mail.strip() == '':
            raise BadRequestException('Validation error', 'Invalid email')

        user: User = User.objects(email=mail.strip().lower()).first()
        if user is None:
            raise NotFoundException(description="no user with mail {}".format(mail))

        user.resetDate = datetime.now(timezone.utc)
        user.resetKey = RandomUtil.generate_reset_key()

        return user.save()

    def complete_password_reset(self, new: str, key: str) -> User:
        JsonLogger.application(DEBUG, "run complete_password_reset on UserService")

        if key is None or key == '':
            raise BadRequestException('Empty value', 'Not value found on key parameter')

        user: User = User.objects(resetKey=key).first()
        if user is None:
            raise NotFoundException('Invalid key')

        user.set_password(new)
        user.save()
        JsonLogger.application(INFO, 'update the password of user {} with an resetKey'.format(user.id))

        return user

    def save_account(self, data: dict, headers: dict) -> User:
        JsonLogger.application(DEBUG, "run save_account on UserService")

        identifier = self.security_utils.getCurrentUserId(headers)
        if not identifier:
            raise UnauthorizedException()

        try:
            user: User = User.objects(id=identifier).first()
            if user is None:
                raise NotFoundException()
        except Exception:
            raise NotFoundException()

        user.firstName = data.get('firstName', user.firstName)
        user.lastName = data.get('lastName', user.lastName)
        user.langKey = data.get('langKey', user.langKey)

        if user.createdDate is None:
            user.createdDate = datetime.now(timezone.utc)

        if 'email' in data:
            if AuthoritiesConstants.ADMIN in user.authorities:
                user.email = data.get('email', user.email).lower().strip()
            else:
                user.email = data.get('email', user.email).lower().strip()
                user.login = user.email

        return user.save()
