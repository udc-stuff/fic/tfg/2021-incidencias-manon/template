import os
import traceback
from typing import List

from backoff_utils import apply_backoff, backoff
from backoff_utils import strategies
import requests
from logging import DEBUG, INFO

from mongoengine import NotUniqueError, DoesNotExist, OperationError, LookUpError, InvalidQueryError

from app import BadRequestException
from app.domain.ComplexHealthcheck import ComplexHealthcheck
from app.domain.BasicHealthcheck import BasicHealthcheck
from app.rest.vm.ComplexHealthcheckVm import ComplexHealthcheckVM
from app.rest.vm.BasicHealthcheckVm import BasicHealthcheckVM
from app.util.JsonLogger import JsonLogger
from app.security.SecurityUtils import SecurityUtils
from app.domain.User import User
from app.errors.UnauthorizedException import UnauthorizedException
from app.errors.NotFoundException import NotFoundException


class HealthcheckService:
    security_utils = SecurityUtils()

    """singleton implementation for this class"""

    def __new__(cls, *args, **kwargs):
        try:
            cls.__instance
        except AttributeError:
            cls.__instance = super(HealthcheckService, cls).__new__(cls)
        return cls.__instance

    def update_basic(self, basic_vm: BasicHealthcheckVM) -> BasicHealthcheck:
        JsonLogger.application(DEBUG, "run update_basic on HealthcheckService")

        basic = BasicHealthcheck.objects(component=basic_vm.component).first()
        if basic is not None and str(basic.id) != basic_vm.id and str(basic.url) == basic_vm.url \
                and str(basic.method) == basic_vm.method and str(basic.status) == basic_vm.status:
            raise BadRequestException('Duplicated values', 'the healthcheck already exists')

        basic = BasicHealthcheck.objects(id=basic_vm.id).first()
        if basic is None:
            JsonLogger.application(INFO, 'try to update a non existent entity (id: {})'.format(basic_vm.id),
                                   traceback.format_exc())
            raise NotFoundException(description='Not found resource with id {}'.format(basic_vm.id))

        try:
            basic = basic_vm.update_basic(basic)
            return basic.save()

        except DoesNotExist:
            JsonLogger.application(INFO, 'try to update a non existent entity (id: {})'.format(basic_vm.id),
                                   traceback.format_exc())
            raise NotFoundException(description='Not found resource with id {}'.format(basic_vm.id))
        except (OperationError, LookUpError, InvalidQueryError):
            JsonLogger.application(INFO, 'failed on update an entity with id {}'.format(basic_vm.id),
                                   traceback.format_exc())
            raise BadRequestException()

    def update_complex(self, complex_vm: ComplexHealthcheckVM) -> ComplexHealthcheck:
        JsonLogger.application(DEBUG, "run update_complex on HealthcheckService")

        complx = ComplexHealthcheck.objects(component=complex_vm.component).first()
        if complx is not None and str(complx.id) != complex_vm.id and str(complx.code) == complex_vm.code:
            raise BadRequestException('Duplicated values', 'the code of  the complex healthcheck already exists')

        complx = ComplexHealthcheck.objects(id=complex_vm.id).first()
        if complx is None:
            JsonLogger.application(INFO, 'try to update a non existent entity (id: {})'.format(complex_vm.id),
                                   traceback.format_exc())
            raise NotFoundException(description='Not found resource with id {}'.format(complex_vm.id))

        try:
            complx = complex_vm.update_complex(complx)
            return complx.save()

        except DoesNotExist:
            JsonLogger.application(INFO, 'try to update a non existent entity (id: {})'.format(complex_vm.id),
                                   traceback.format_exc())
            raise NotFoundException(description='Not found resource with id {}'.format(complex_vm.id))
        except (OperationError, LookUpError, InvalidQueryError):
            JsonLogger.application(INFO, 'failed on update an entity with id {}'.format(complex_vm.id),
                                   traceback.format_exc())
            raise BadRequestException()

    def delete_basic(self, identifier: str) -> BasicHealthcheck:
        JsonLogger.application(DEBUG, "run delete_basic on HealthcheckService")

        basic: BasicHealthcheck = BasicHealthcheck.objects(id=identifier).first()
        if basic is None:
            JsonLogger.application(INFO, 'try to delete a non existent entity (id: {})'.format(identifier))
            raise NotFoundException(description='No basic healthcheck with id {} to delete'.format(identifier))

        basic.delete()
        JsonLogger.application(INFO, 'deleted basic healthcheck with id={}'.format(basic.id))

        return basic

    def delete_complex(self, identifier: str) -> ComplexHealthcheck:
        JsonLogger.application(DEBUG, "run delete_complex on HealthcheckService")

        complx: ComplexHealthcheck = ComplexHealthcheck.objects(id=identifier).first()
        if complx is None:
            JsonLogger.application(INFO, 'try to delete a non existent entity (id: {})'.format(identifier))
            raise NotFoundException(description='No complex healthcheck with id {} to delete'.format(identifier))

        complx.delete()
        JsonLogger.application(INFO, 'deleted complex healthcheck with id={}'.format(complx.id))

        return complx

    def get_basic_healthchecks_by_component(self, component: str, headers: dict) -> List[BasicHealthcheck]:
        JsonLogger.application(DEBUG,
                               'run get_basic_healthchecks_by_component on HealtcheckService (component={})'.format(
                                   component))

        identifier: str = self.security_utils.getCurrentUserId(headers=headers)
        if not identifier:
            raise UnauthorizedException()

        user: User = User.objects(id=identifier).first()
        if user is None:
            raise NotFoundException()

        basics = BasicHealthcheck.objects(component=component)
        if basics is None:
            raise NotFoundException(description='No found healthcheck with component {}'.format(component))

        return basics

    def get_complex_healthchecks_by_component(self, component: str, headers: dict) -> List[ComplexHealthcheck]:
        JsonLogger.application(DEBUG,
                               'run get_complex_healthchecks_by_component on HealtcheckService (component={})'.format(
                                   component))

        identifier: str = self.security_utils.getCurrentUserId(headers=headers)
        if not identifier:
            raise UnauthorizedException()

        user: User = User.objects(id=identifier).first()
        if user is None:
            raise NotFoundException()

        complx = ComplexHealthcheck.objects(component=component)
        if complx is None:
            raise NotFoundException(description='No found healthcheck with component {}'.format(component))

        return complx

    # @apply_backoff(strategy=strategies.Exponential(minimum=1), max_tries=2, max_delay=3600)
    def do_request(self, url: str, method: str, status: int):
        JsonLogger.application(DEBUG, "running a request from basic healthcheck on HealthcheckService")

        tmp = {'get': requests.get, 'post': requests.post, 'put': requests.put, 'delete': requests.delete}
        if method.lower() not in tmp:
            raise Exception('Method dont allowed:' + method)  # Añadir excepcion

        response = tmp[method.lower()](url)

        if response.status_code != status:
            raise Exception('Something went wrong with the basic healthcheck, status code'
                            ' not expected:  {}'.format(response.status_code))

        return response

    def basic_healthcheck(self, url: str, method: str, status: int, retry: bool):
        JsonLogger.application(DEBUG, "run basic healthcheck on HealthcheckService")

        try:
            if retry:
                response = backoff(self.do_request,
                                   args=[url, method, status],
                                   max_tries=3,
                                   max_delay=3600,
                                   strategy=strategies.Exponential)
            else:
                response = self.do_request(url, method, status)
        except:
            raise Exception('Something went wrong with the basic healthcheck')
            # Lanzar incidencia instatus --> +adelante se hará en el InstatusComunicationService NO AQUI

        return response

    def do_complex_request(self, code: str):
        JsonLogger.application(DEBUG, "running a request from complex healthcheck on HealthcheckService")

        try:
            response = exec(code)
            return response
            # return {"Status": "The code was executed correctly and without failures"}
        except:
            raise Exception('Something went wrong when the code was run')

    def child(self, code: str, retry: bool):
        # Execute code from user complex healthcheck
        print("CODE------>", code)

        try:
            if retry:
                response = backoff(self.do_complex_request,
                                   args=[code],
                                   max_tries=3,
                                   max_delay=3600,
                                   strategy=strategies.Exponential)
            else:
                response = self.do_complex_request(code)
        except:
            raise Exception('Something went wrong with the complex healthcheck')
            # Lanzar incidencia instatus --> +adelante se hará en el InstatusComunicationService NO AQUI

        return response

    def complex_healthcheck(self, code: str, retry: bool):
        JsonLogger.application(DEBUG, "run complex healthcheck on HealthcheckService")

        try:
            newpid = os.fork()
        except OSError:
            exit("Could not create a child process")

        if newpid == 0:
            return self.child(code, retry)
        else:
            os.wait()
            JsonLogger.application(DEBUG, "Child process %d exited" % newpid)
            JsonLogger.application(DEBUG, "Parent process %d exiting after child has exited" % (os.getpid()))

    def save_basic(self, basic_vm: BasicHealthcheckVM, headers: dict):
        JsonLogger.application(DEBUG, "saving basic healthcheck on HealthcheckService")

        identifier: str = self.security_utils.getCurrentUserId(headers=headers)
        if not identifier:
            raise UnauthorizedException()

        user: User = User.objects(id=identifier).first()
        if user is None:
            raise NotFoundException()

        basic = BasicHealthcheck.objects(name=basic_vm.name).first()
        if basic is not None and str(basic.id) != basic_vm.id and str(basic.component) == basic_vm.component and \
                str(basic.url) == basic_vm.url and str(basic.method) == basic_vm.method \
                and str(basic.status) == basic_vm.status:
            raise BadRequestException('Duplicated values', 'the healthcheck already exists')

        basic: BasicHealthcheck = basic_vm.get_basic()

        try:
            basic = basic.save()
            return basic
        except NotUniqueError:
            raise BadRequestException('Duplicate Healthcheck Error')

    def save_complex(self, complex_vm: ComplexHealthcheckVM, headers: dict):
        JsonLogger.application(DEBUG, "saving complex healthcheck on HealthcheckService")

        identifier: str = self.security_utils.getCurrentUserId(headers=headers)
        if not identifier:
            raise UnauthorizedException()

        user: User = User.objects(id=identifier).first()
        if user is None:
            raise NotFoundException()

        complx = ComplexHealthcheck.objects(code=complex_vm.code).first()
        if complx is not None and str(complx.id) != complex_vm.id and str(complx.component) == complx.component:
            raise BadRequestException('Duplicated values', 'the code of the complex healthcheck already exists')

        complx: ComplexHealthcheck = complex_vm.get_basic()

        try:
            complx = complx.save()
            return complx
        except NotUniqueError:
            raise BadRequestException('Duplicate Healthcheck Error')
