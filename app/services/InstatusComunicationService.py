import json
import requests

from app import crontab
from backoff_utils import apply_backoff
from backoff_utils import strategies
from app import celery_app
from datetime import datetime, timezone
from flask import jsonify, Response
from flask_crontab import Crontab

from app.domain.User import User
from app.security.SecurityUtils import SecurityUtils
from logging import DEBUG, INFO

from app.domain.ComplexHealthcheck import ComplexHealthcheck
from app.domain.BasicHealthcheck import BasicHealthcheck
from app.rest.vm.ComplexHealthcheckVm import ComplexHealthcheckVM
from app.rest.vm.BasicHealthcheckVm import BasicHealthcheckVM
from app.services.HealthcheckService import HealthcheckService
from app.errors.NotFoundException import NotFoundException
from app.errors.UnauthorizedException import UnauthorizedException

from app.util.JsonLogger import JsonLogger
from config import InStatusConfiguration

healthcheck_service = HealthcheckService()


def get_healthcheck_by_id(identificator: str):
    JsonLogger.application(DEBUG, 'run get_basic_healthcheck_by_id on InstatusComunicationService'
                                  ' (id={})'.format(identificator))

    health_basic: BasicHealthcheck = BasicHealthcheck.objects(id=identificator).first()
    if health_basic is None:
        health_complex: ComplexHealthcheck = ComplexHealthcheck.objects(id=identificator).first()
        if health_complex is not None:
            return health_complex
        else:
            raise NotFoundException(description='No found healthcheck with id {}'.format(identificator))

    return health_basic


@celery_app.task(name="app.service.instatus_service.healthcheck_with_incident", ignore_result=True,
                 max_retries=3)
def healthcheck_with_incident(identificator: str) -> Response:
    JsonLogger.application(DEBUG, 'run do_healthcheck_with_incident on InstatusComunicationService '
                                  '(id={})'.format(identificator))

    health = get_healthcheck_by_id(identificator)

    try:
        if isinstance(health, BasicHealthcheck):
            print("RESPONSE BASIC")
            response = healthcheck_service.basic_healthcheck(health.url, health.method,
                                                             health.status, health.retry)
        else:
            print("RESPONSE COMPLEX")
            response = healthcheck_service.complex_healthcheck(health.code, health.retry)

        if not health.active:
            health.active = True
            health.save()
    except:
        print("ESTADO ACTUAL DEL HEALTH CHECK ..............", health.active)
        if health.active:
            access_token = InStatusConfiguration.API_KEY
            page_id = InStatusConfiguration.PAGE_ID
            my_headers = {'Authorization': 'Bearer ' + access_token,
                          'Content-Type': 'application/json'}
            my_url = "https://api.instatus.com/v1/" + page_id + "/incidents"

            my_data = {"name": health.incident,
                       "message": "We're currently investigating an issue with the Website",
                       "components": [health.component],
                       "started": str(datetime.now(timezone.utc)),
                       "status": "INVESTIGATING",
                       "statuses": [{
                           "id": health.component,
                           "status": "OPERATIONAL"
                       }]
                       }

            response = requests.post(url=my_url, json=my_data, headers=my_headers)

            health.active = False
            health.save()
            print("ESTADO FINAL DEL HEALTH CHECK ..............", health.active)
        else:
            return Response("", status=200)

        print("RESPONSE URL      ---------->", response.url, "\n",
              "STATUS CODE       ---------->", response.status_code, "\n",
              "RESPONSE HEADERS  ---------->", response.headers, "\n",
              "RESPONSE CONTENT  ---------->", response.content, "\n")

    print("RESPONSE      --------------> ", response)

    return response


class InstatusComunicationService:
    security_utils = SecurityUtils()

    """singleton implementation for this class"""

    def __new__(cls, *args, **kwargs):
        try:
            cls.__instance
        except AttributeError:
            cls.__instance = super(InstatusComunicationService, cls).__new__(cls)
        return cls.__instance

    def my_scheduled_job(self):
        ids = []
        for basicHealthcheck in BasicHealthcheck.objects:
            ids.append(str(basicHealthcheck.id))

        for complexHealthcheck in ComplexHealthcheck.objects:
            ids.append(str(complexHealthcheck.id))

        JsonLogger.application(DEBUG, ">>>>>>>>>>>>>>>>>>>>>> IDS <<<<<<<<<<<<<<<<<<<<<<", ids)

        for identificator in ids:
            print(identificator)
            from celery.utils.log import get_logger
            logger = get_logger(__name__)
            try:
                data = healthcheck_with_incident.apply_async(retry=False, args=(identificator,))
                JsonLogger.application(DEBUG, 'Tipo {}'.format(type(data)))
            except Exception as e:
                logger.exception('Excepcion %r', e)

    @apply_backoff(strategy=strategies.Exponential, max_tries=3, max_delay=3600)
    def get_incidents(self) -> Response:
        JsonLogger.application(DEBUG, 'run get_incidents on InstatusComunicationService')

        access_token = InStatusConfiguration.API_KEY
        page_id = InStatusConfiguration.PAGE_ID
        my_headers = {'Authorization': 'Bearer ' + access_token,
                      'Content-Type': 'application/json'}
        my_url = "https://api.instatus.com/v1/" + page_id + "/incidents"

        response = requests.get(url=my_url, headers=my_headers)

        data = response.json()
        return data

    @apply_backoff(strategy=strategies.Exponential, max_tries=3, max_delay=3600)
    def get_components(self) -> Response:
        JsonLogger.application(DEBUG, 'run get_components on InstatusComunicationService')

        access_token = InStatusConfiguration.API_KEY
        page_id = InStatusConfiguration.PAGE_ID
        my_headers = {'Authorization': 'Bearer ' + access_token,
                      'Content-Type': 'application/json'}
        my_url = "https://api.instatus.com/v1/" + page_id + "/components"

        response = requests.get(url=my_url, headers=my_headers)

        data = response.json()
        return data
