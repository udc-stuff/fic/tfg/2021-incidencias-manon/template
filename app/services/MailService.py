import os
import lxml.html
from logging import DEBUG, INFO
from flask_mail import Message
from string import Template

from config import MailConfiguration
from app import mail
from app.util.JsonLogger import JsonLogger


class MailService:
    """singleton implementation for this class"""

    def __new__(cls, *args, **kwargs):
        try:
            cls.__instance
        except AttributeError:
            JsonLogger.application(DEBUG, 'Create a MailService instance')  # Application log
            cls.__instance = super(MailService, cls).__new__(cls)
        return cls.__instance

    def __init__(self):

        self.template_dir = MailConfiguration.TEMPLATE_DIR
        self.base_url = MailConfiguration.BASE_URL

        self.app_name = MailConfiguration.APP_NAME

        self.mail = mail

    def __read_template(self, filename: str, lang_key: str = '') -> Template:
        file = os.path.join(self.template_dir, lang_key, filename)
        if not os.path.exists(file):  # not complete langKey
            JsonLogger.application(INFO, 'not {} template for lang {}'.format(filename, lang_key))
            lang_key = lang_key[:2]
            file = os.path.join(self.template_dir, lang_key, filename)
            if not os.path.exists(file):  # not lang part
                JsonLogger.application(INFO, 'not {} template for lang {}'.format(filename, lang_key))
                file = os.path.join(self.template_dir, filename)

        with open(file, 'r', encoding='utf-8') as template_file:
            template_file_content = template_file.read()

        return Template(template_file_content)

    def __get_title(self, html_str: str) -> str:
        t = lxml.html.document_fromstring(html_str)
        return t.find(".//title").text

    def send_email_text(self, subject, message_text, to, bcc=None, reply_to=None):
        JsonLogger.application(INFO, 'subject:{}, to={}, bcc={}'.format(subject, to, bcc, reply_to))
        msg = Message(subject, sender=("PhotoILike", MailConfiguration.USERNAME), recipients=[to], bcc=bcc,
                      reply_to=reply_to, charset="utf-8")
        msg.body = message_text

        JsonLogger.application(DEBUG, 'text message to send')
        self.mail.send(msg)
        JsonLogger.application(DEBUG, 'text message was sent')

    def send_email_html(self, subject, message_html, to, bcc=None, reply_to=None):
        JsonLogger.application(INFO, 'subject:{}, to={}, bcc={}'.format(subject, to, bcc, reply_to))
        msg = Message(subject, sender=("PhotoILike", MailConfiguration.USERNAME), recipients=[to], bcc=bcc,
                      reply_to=reply_to, charset="utf-8")
        msg.html = message_html

        JsonLogger.application(DEBUG, 'html message to send')
        self.mail.send(msg)
        JsonLogger.application(DEBUG, 'html message was sent')

    def send_activation_email(self, to: str, activation_key: str, lang_key: str):
        JsonLogger.application(DEBUG, 'Sending activation email to {} with langKey {}'.format(to, lang_key))

        template: Template = self.__read_template('activationEmail.html', lang_key)

        email_content = template.substitute(
            BASE_URL=self.base_url, ACTIVATION_KEY=activation_key
        )

        subject = self.__get_title(email_content)

        self.send_email_html(subject, email_content, to)

    def send_password_reset_email(self, to: str, activation_key: str, lang_key: str):
        JsonLogger.application(DEBUG, 'Sending reset password email to {} with langKey {}'.format(to, lang_key))

        template: Template = self.__read_template('passwordResetEmail.html', lang_key)

        email_content = template.substitute(
            BASE_URL=self.base_url, ACTIVATION_KEY=activation_key
        )

        subject = self.__get_title(email_content)

        self.send_email_html(subject, email_content, to)
