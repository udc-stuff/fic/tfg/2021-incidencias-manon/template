from logging import DEBUG, INFO

from functools import wraps
from urllib import request

from flask import request

from app.errors.ForbiddenException import ForbiddenException
from app.errors.UnauthorizedException import UnauthorizedException
from app.errors.UnsupportedMediaTypeException import UnsupportedMediaTypeException
from app.security.SecurityUtils import SecurityUtils
from app.util.JsonLogger import JsonLogger

security_utils = SecurityUtils()


def PreAuthorize(role: str):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            JsonLogger.application(DEBUG, 'Start Preauthorize Decorator')

            if not security_utils.isAuthenticated(dict(request.headers)):
                JsonLogger.application(INFO, '[REJECTED] wrapper denied access because is an unauthorized request')
                raise UnauthorizedException()

            if not security_utils.isCurrentUserInRole(dict(request.headers), role):
                JsonLogger.application(INFO, '[REJECTED] wrapper denied access because is a forbidden request')
                raise ForbiddenException()

            JsonLogger.application(DEBUG, 'Has the right role ({})'.format(role))

            v = f(*args, **kwargs)
            # JsonLogger.application(DEBUG, 'After PreAuthorize request')
            return v
        return wrapper
    return decorator


def RequiredContentType(content_type: str):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            JsonLogger.application(DEBUG, 'Start RequiredContentType Decorator')

            if request.content_type != content_type:
                msg = '[REJECTED] Invalid content type. Has {} but {} was required'\
                    .format(request.content_type, content_type)
                JsonLogger.application(INFO, msg)
                raise UnsupportedMediaTypeException(description=msg)

            v = f(*args, **kwargs)
            # JsonLogger.application(DEBUG, 'After RequiredContentType request')
            return v
        return wrapper
    return decorator
