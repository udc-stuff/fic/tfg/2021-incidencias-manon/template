from logging import DEBUG

from app.domain.User import User
from app.security.AuthoritiesConstants import AuthoritiesConstants
from app.security.jwt.TokenProvider import TokenProvider
from app.util.JsonLogger import JsonLogger


class SecurityUtils:
    token_provider = TokenProvider()

    """singleton implementation for this class"""
    def __new__(cls, *args, **kwargs):
        try:
            cls.__instance
        except AttributeError:
            JsonLogger.application(DEBUG, 'Create a SecurityUtils instance')
            cls.__instance = super(SecurityUtils, cls).__new__(cls)
        return cls.__instance

    def __init__(self):
        pass

    def getCurrentUserId(self, headers: dict) -> str:
        try:
            token: str = self.token_provider.resolve_token(headers)

            return self.token_provider.get_id_from_token(token)
        except Exception:
            return ''

    def getCurrentLogin(self, headers: dict) -> str:
        try:
            token: str = self.token_provider.resolve_token(headers)

            return self.token_provider.get_login_name_from_token(token)
        except Exception:
            return ''

    def isAuthenticated(self, headers: dict) -> bool:
        token: str = self.token_provider.resolve_token(headers)

        return self.token_provider.validate_token(token)

    def isCurrentUserInRole(self, headers: dict, role: str) -> bool:
        token: str = self.token_provider.resolve_token(headers)

        if not token or not self.token_provider.validate_token(token):
            return False

        # this will be the place to test sensitive roles on microservice which check the pair (id, roles) on the storage
        try:
            # system doesnt have an account
            if role == AuthoritiesConstants.SYSTEM:
                return role in self.token_provider.get_roles_from_token(token)

            identifier: str = self.token_provider.get_id_from_token(token)
            u: User = User.objects(id=identifier).first()
            return role in u.authorities
        except Exception:
            return False

        # this service can check role on storage
        # try:
        #     return role in self.token_provider.get_roles_from_token(token)
        # except Exception:
        #     return False

    def getHeaderAndPayload(self, headers: dict) -> str:
        token: str = self.token_provider.resolve_token(headers)

        if not token or not self.token_provider.validate_token(token):
            return ''

        try:
            return '.'.join(token.split('.', 2)[:2])
        except Exception:
            return ''
