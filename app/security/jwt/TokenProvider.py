import traceback

import jwt
import logging
from datetime import datetime
from typing import List

from app.util.JsonLogger import JsonLogger
from config import BasicConfiguration

from app.errors.UnprocessableEntityException import UnprocessableEntityException


class ExpiredJwtException(Exception):
    pass


class TokenProvider:

    ISSUER = '{}:{}'.format(BasicConfiguration.APP_NAME, BasicConfiguration.VERSION)

    AUTHORIZATION_HEADER = "Authorization"
    AUTHORIZATION_HEADER_PREFIX = "Bearer "

    __private_key: str

    __algorithm: str = 'HS512'
    ALGORITHMS: List[str] = [__algorithm]

    __token_validity_in_seconds: int = 90000  # 25 hours

    __token_validity_in_seconds_for_remember_me: int = 2592000  # 30 days

    """singelton implementation for this class"""
    def __new__(cls, *args, **kwargs):
        try:
            cls.__instance
        except AttributeError:
            JsonLogger.application(logging.INFO, 'create a TokenProvider instance')
            cls.__instance = super(TokenProvider, cls).__new__(cls)
        return cls.__instance

    def __init__(self, private_key: str = None, algorithm: str = None, validity: int = None, validity_remember: int = None):
        if private_key is not None:
            self.__private_key = private_key
        if algorithm is not None:
            self.__algorithm = algorithm
            self.ALGORITHMS.append(self.__algorithm)
        if validity is not None:
            self.__token_validity_in_seconds = validity
        if validity_remember is not None:
            self.__token_validity_in_seconds_for_remember_me = validity_remember

    @staticmethod
    def __generate_payload(identifier: str, login: str, exp: float, iat: float, roles: List[str]) -> dict:
        return {
            'iss': TokenProvider.ISSUER,
            'id': identifier,
            'sub': login,
            'exp': exp,
            'iat': iat,
            'jti': '{}-{}'.format(identifier, iat),
            'roles': roles
        }

    @staticmethod
    def __check_payload_attributes(payload: dict) -> bool:
        if 'id' not in payload:
            return False
        if 'sub' not in payload:
            return False
        if 'exp' not in payload:
            return False

        return True

    def create_token(self, identifier: str, login: str, remember: bool, roles: List[str], key=None) -> str:
        now = datetime.timestamp(datetime.now())

        if remember:
            exp = now + self.__token_validity_in_seconds_for_remember_me
        else:
            exp = now + self.__token_validity_in_seconds

        payload = self.__generate_payload(identifier, login, exp, iat=now, roles=roles)
        return jwt.encode(payload, key or self.__private_key, self.__algorithm).decode('utf8')

    @staticmethod
    def __get_attr_from_token(token: str, attr: str):
        try:
            payload = jwt.decode(token, verify=False, algorithms=TokenProvider.ALGORITHMS)
            if attr not in payload:
                payload[attr] = ''
                raise UnprocessableEntityException(title="Invalid token")

        except jwt.exceptions.InvalidSignatureError as e:
            JsonLogger.application(logging.INFO, 'Invalid JWT signature. Token={}'.format(token), traceback.format_exc())
            raise e
        except jwt.exceptions.DecodeError as e:
            JsonLogger.application(logging.INFO, 'Unsupported JWT. Token={}'.format(token), traceback.format_exc())
            raise e
        except UnprocessableEntityException as e:
            JsonLogger.application(logging.INFO, 'JWT payload is invalid. Token={}'.format(token), traceback.format_exc())
            raise e

        return payload[attr]


    @classmethod
    def get_login_name_from_token(cls, token: str) -> str:
        return TokenProvider.__get_attr_from_token(token, 'sub')

    @classmethod
    def get_id_from_token(cls, token: str) -> str:
        return TokenProvider.__get_attr_from_token(token, 'id')

    @classmethod
    def get_roles_from_token(cls, token: str) -> List[str]:
        return TokenProvider.__get_attr_from_token(token, 'roles')

    def validate_token(self, token: str, key=None) -> bool:
        try:
            payload = jwt.decode(token, key or self.__private_key, algorithms=[self.__algorithm])

            if not self.__check_payload_attributes(payload):
                raise UnprocessableEntityException(title="Invalid token")

            now = datetime.timestamp(datetime.now())
            if now > payload['exp']:
                raise ExpiredJwtException()

            return True
        except jwt.exceptions.InvalidSignatureError as e:
            JsonLogger.application(logging.INFO, 'Invalid JWT signature token={}'.format(token), traceback.format_exc())
        except jwt.exceptions.DecodeError as e:
            JsonLogger.application(logging.INFO, 'Unsupported JWT token={}'.format(token), traceback.format_exc())
        except UnprocessableEntityException as e:
            JsonLogger.application(logging.INFO, 'JWT payload is invalid token={}'.format(token), traceback.format_exc())
        except ExpiredJwtException as e:
            JsonLogger.application(logging.INFO, 'Expired JWT token', traceback.format_exc())
        except Exception as e:
            JsonLogger.application(logging.ERROR, 'Cannot validate json token={}'.format(token), traceback.format_exc())

        return False

    @classmethod
    def resolve_token(cls, headers: dict) -> str:
        bearer_token: str = headers.get(TokenProvider.AUTHORIZATION_HEADER, '')
        if not bearer_token:
            JsonLogger.application(logging.INFO, 'not found {} on headers'.format(TokenProvider.AUTHORIZATION_HEADER))
            return ''

        if not bearer_token.startswith(TokenProvider.AUTHORIZATION_HEADER_PREFIX):
            JsonLogger.application(logging.INFO,
                                   'Bearer token do not start with {}'.format(TokenProvider.AUTHORIZATION_HEADER_PREFIX))
            return ''

        return bearer_token[len(TokenProvider.AUTHORIZATION_HEADER_PREFIX):]
