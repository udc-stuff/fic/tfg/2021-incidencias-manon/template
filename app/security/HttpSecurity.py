import re
from flask import request

from logging import DEBUG, INFO

from app.errors.ForbiddenException import ForbiddenException
from app.errors.NotFoundException import NotFoundException
from app.errors.UnauthorizedException import UnauthorizedException
from app.main.Routes import index
from app.security.SecurityUtils import SecurityUtils
from app.util.JsonLogger import JsonLogger
from config import SecurityConfiguration

security_utils = SecurityUtils()


def before_request_http_security():
    # check front end
    for item in SecurityConfiguration.WEB_IGNORE:
        if re.fullmatch(item, request.path) and request.method in item['methods']:
            JsonLogger.application(DEBUG, '[ACCEPTED] web-ignore: "{}" match with "{}"'.format(request.path, item))
            return

    url_match = False
    methods = []
    # check auth request
    for item in SecurityConfiguration.AUTHORIZE_REQUEST:
        if re.fullmatch('^' + item['url'], request.path):
            url_match = True
            methods.append(item['methods'])
            if request.method in item['methods']:
                if not item['auth']:  # not authentication required
                    JsonLogger.application(DEBUG,
                                           '[ACCEPTED] authorize-request: "{}" match with "{}"'
                                           .format(request.path, item))
                    return

                if not security_utils.isAuthenticated(dict(request.headers)):  # authentication required
                    JsonLogger.application(INFO, '[REJECTED] authorize-request: "{}" match with "{}" '
                                                 'but not authenticated '
                                                 '[header and payload token = "{}"]'
                                           .format(request.path, item,
                                                   security_utils.getHeaderAndPayload(dict(request.headers)))
                                           )
                    raise UnauthorizedException()

                # specific role required
                if 'role' in item and not security_utils.isCurrentUserInRole(dict(request.headers), item['role']):
                    JsonLogger.application(INFO,
                                           '[REJECTED] authorize-request: "{}" match with "{}" '
                                           'but has not specific role '
                                           '[header and payload token "{}"]'
                                           .format(request.path, item,
                                                   security_utils.getHeaderAndPayload(dict(request.headers)))
                                           )
                    raise ForbiddenException()

                # matched request and satisfied the requisites
                JsonLogger.application(DEBUG,
                                       '[ACCEPTED] authorize-request: "{}" match with "{}"'.format(request.path, item))
                return

    # url match but no method this means Method not allowed instead of not found
    description = None
    if url_match:
        if len(methods) > 0:
            description = 'The requested resource was not found on this server.' + \
                          'Tip: You are using the {} method, the request may have to be made with another method.' \
                              .format(request.method)

            description += ' Try it with one of this {}'.format(methods)
        raise NotFoundException(description)

    # requested not matched
    if SecurityConfiguration.REJECT_NOT_MATCHED:
        JsonLogger.application(INFO,
                               '[REJECTED] "{}" does not match with any path of SecurityConfiguration'
                               .format(request.path))
        raise NotFoundException(description=description)
    else:
        JsonLogger.application(INFO,
                               '[ACCEPTED] authorize-request: {} does not match with any path. Return the main page'
                               .format(request.path))
        return index()
