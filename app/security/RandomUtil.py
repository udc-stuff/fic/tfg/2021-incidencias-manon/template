from random import choice
from string import ascii_uppercase
from string import ascii_lowercase
from string import digits


class RandomUtil:

    DEF_COUNT: int = 30

    @staticmethod
    def generate_password() -> str:
        value = (''.join(choice(ascii_uppercase + ascii_lowercase + digits) for _ in range(RandomUtil.DEF_COUNT)))
        return value

    @staticmethod
    def generate_activation_key() -> str:
        value = (''.join(choice(digits) for _ in range(RandomUtil.DEF_COUNT)))
        return str(value)

    @staticmethod
    def generate_reset_key() -> str:
        value = (''.join(choice(digits) for _ in range(RandomUtil.DEF_COUNT)))
        return str(value)
