import re


class AuthoritiesConstants:

    @classmethod
    def all(cls):
        """
        Returns all constant values in class with the attribute requirements:
         - only uppercase letters and underscores
         - must begin and end with a letter
        """

        regex = r'^[A-Z][A-Z_]*[A-Z]$'
        return [kv[1] for kv in cls.__dict__.items() if re.match(regex, kv[0])]

    SYSTEM: str = 'ROLE_SYSTEM'
    ADMIN: str = 'ROLE_ADMIN'
    USER: str = 'ROLE_USER'
    ANONYMOUS: str = 'ROLE_ANONYMOUS'


#
# ROLE_SYSTEM: it's for system's service requests
# ROLE_ADMIN: administration user
# ROLE_USER: basic user with restricted resources
# ROLE_ANONYMOUS: user cannot get an jwt token
#
