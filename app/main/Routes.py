from app.main import bp


@bp.route('/', methods=['GET'])
def index():
    return bp.send_static_file('index.html')
