import os
from flask import Blueprint


def __get_static_folder():
    return os.path.join(os.getcwd(), 'static')


bp = Blueprint('main', __name__, static_folder=__get_static_folder(), static_url_path='')

from app.main import Routes
