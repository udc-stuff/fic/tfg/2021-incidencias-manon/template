import logging
import os
import sys
import six
import platform
import importlib

from datetime import datetime, timezone

from typing import Dict

from flask import jsonify, request

from app.rest import bp
from app.security.AuthoritiesConstants import AuthoritiesConstants
from app.security.SecurityUtils import SecurityUtils
from app.util.JsonLogger import JsonLogger

from config import BasicConfiguration

security_utils = SecurityUtils()


class Environment:

    def __init__(self, include_os=True, include_python=True, include_process=True):
        self.functions = {}

        if include_os:
            self.functions['os'] = self.__get_os
        if include_python:
            self.functions['python'] = self.__get_python
        if include_process:
            self.functions['process'] = self.__get_process

    def dump_environment(self):
        data = {}
        for (name, func) in six.iteritems(self.functions):
            data[name] = func()

        return data

    def __get_python(self):
        result = {
            'version': sys.version,
            'executable': sys.path,
            'pythonpath': sys.path,
            'version_info': {
                'major': sys.version_info.major,
                'minor': sys.version_info.minor,
                'micro': sys.version_info.micro,
                'releaselevel': sys.version_info.releaselevel,
                'serial': sys.version_info.serial
            }
        }

        if importlib.util.find_spec('pkg_resources'):
            import pkg_resources
            packages = dict([(p.project_name, p.version) for p in pkg_resources.working_set])
            result['packages'] = packages

        return result

    def __get_os(self):
        return {'platform': sys.platform,
                'name': os.name,
                'uname': platform.uname()}

    def __get_login(self):    # pragma: no cover
        # Based on https://github.com/gitpython-developers/GitPython/pull/43/
        # Fix for 'Inappopropirate ioctl for device' on posix systems.
        if os.name == "posix":    # pragma: no cover
            import pwd
            username = pwd.getpwuid(os.geteuid()).pw_name
        else:
            username = os.environ.get('USER', os.environ.get('USERNAME', 'UNKNOWN'))
            if username == 'UNKNOWN' and hasattr(os, 'getlogin'):
                username = os.getlogin()
        return username

    def __get_process(self):
        return {'argv': sys.argv,
                'cwd': os.getcwd(),
                'user': self.__get_login(),
                'pid': os.getpid(),
                'environ': self.__safe_dump(os.environ)}

    def __safe_dump(self, dictionary):    # pragma: no cover
        result = {}
        for key in dictionary.keys():
            if 'key' in key.lower() or 'token' in key.lower() or 'pass' in key.lower():
                # Try to avoid listing passwords and access tokens or keys in the output
                result[key] = "********"
            else:
                try:
                    import json
                    json.dumps(dictionary[key])
                    result[key] = dictionary[key]
                except TypeError:
                    pass
        return result


@bp.route('/healthcheck', methods=['GET'])
def get_healthcheck():
    JsonLogger.application(logging.DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    data: Dict = {
        'hostname': platform.node(),
        'timestamp': datetime.now(timezone.utc).isoformat(),
        'app-name': BasicConfiguration.APP_NAME,
        'version': BasicConfiguration.VERSION
    }

    if security_utils.isCurrentUserInRole(request.headers, AuthoritiesConstants.ADMIN):
        environment = Environment()
        data['environment'] = environment.dump_environment()

    response = jsonify(data)
    response.status_code = 200
    return response
