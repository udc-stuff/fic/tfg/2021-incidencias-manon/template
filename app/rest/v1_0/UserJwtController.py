import json
from logging import DEBUG

from flask import request, Response

from app import JsonLogger, BadRequestException
from app.domain.User import User
from app.errors.UnauthorizedException import UnauthorizedException
from app.rest.v1_0 import bp
from app.rest.vm.LoginVm import LoginVM
from app.security.AccessDecorators import RequiredContentType
from app.security.AuthoritiesConstants import AuthoritiesConstants
from app.security.jwt.TokenProvider import TokenProvider
from app.util.utils import check_required_keys, check_outside_keys

token_provider = TokenProvider()


@bp.route('/authenticate', methods=['POST'])
@RequiredContentType('application/json')
def authorize():
    """
    Authenticate the user with login and password

    body:
     - username (Required)
     - password (Required)
     - remember_me (Optional)
    :return:
     - Token id of the authenticated user
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['username', 'password'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found the following attributes: {}'.format(
                                      ', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['username', 'password', 'remember_me'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    login = LoginVM(**request.json)
    user: User = User.objects(login=login.username).first()
    if user is None:
        raise BadRequestException('Validation error', 'Invalid pair username,password')

    tmp = User(password=user.password)
    if not tmp.check_password(login.password):
        raise BadRequestException('Validation error', 'Invalid pair username,password')

    if not user.activated:
        raise UnauthorizedException(description="User {} was not activated".format(login.username))

    if AuthoritiesConstants.ANONYMOUS in user.authorities:
        raise UnauthorizedException(description='User {} is anonymous. it means no authentication for this user'
                                    .format(login.username))

    token = token_provider.create_token(str(user.id), user.login, login.remember_me,
                                        user.authorities)

    data: dict = {'id_token': token}

    return Response(
        json.dumps(data),
        status=200,
        mimetype="application/json",
        headers={
            token_provider.AUTHORIZATION_HEADER:
                '{}{}'.format(token_provider.AUTHORIZATION_HEADER_PREFIX, data['id_token'])
        }
    )
