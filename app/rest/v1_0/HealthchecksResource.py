import requests
from logging import DEBUG

from flask import request, jsonify, Response
from datetime import datetime, timezone
from app import APP_NAME, BadRequestException, celery_app
from app.rest.v1_0 import bp
from app.domain.User import User

from app.domain.ComplexHealthcheck import ComplexHealthcheck
from app.rest.vm.ComplexHealthcheckVm import ComplexHealthcheckVM
from app.domain.BasicHealthcheck import BasicHealthcheck
from app.rest.vm.BasicHealthcheckVm import BasicHealthcheckVM
from app.security.AccessDecorators import RequiredContentType
from app.services.HealthcheckService import HealthcheckService
from app.services.InstatusComunicationService import InstatusComunicationService
from app.util.HeaderUtils import HeaderUtil
from app.util.JsonLogger import JsonLogger
from app.util.utils import check_required_keys, check_outside_keys
from config import InStatusConfiguration
from app.errors.NotFoundException import NotFoundException
from app.errors.UnauthorizedException import UnauthorizedException

healthcheck_service = HealthcheckService()
instatus_service = InstatusComunicationService()


@bp.route('/basic', methods=['POST'])
@RequiredContentType('application/json')
def basic_healthcheck():
    """
    body:
     - url (Required)
     - method (Required)
     - status (Optional)
     - retry (Required)
    """

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['url', 'method', 'retry'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['url', 'method', 'status', 'retry'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    response = healthcheck_service.basic_healthcheck(data["url"], data["method"],
                                                     int(data.get('status', 200)), data["retry"])

    headers = HeaderUtil.create_alert(APP_NAME, 'healthcheckManagement.testedBasic', response.status_code)
    return Response("", status=200, headers=headers)


@bp.route('/complex', methods=['POST'])
@RequiredContentType('application/json')
def complex_healthcheck():
    """
    body:
     - code (Required) --> Code that runs in the healthcheck
     - retry (Required)
    """

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['code', 'retry'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(
                                      ', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['code', 'retry'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    print("HEALTH COMPLEX CODE ----------------> ", data["code"], "\n",
          "HEALTH COMPLEX BACKOFF  ---------------->", data["retry"], "\n")
    response = healthcheck_service.complex_healthcheck(data["code"], data["retry"])

    headers = HeaderUtil.create_alert(APP_NAME, 'healthcheckManagement.testedComplex', response)
    return Response("", status=200, headers=headers)


@bp.route('/basic/save', methods=['POST'])
@RequiredContentType('application/json')
def save_basic():
    """
    body:
     - component (Required)
     - healthcheck name (Required)
     - url (Required)
     - method (Required)
     - status (Optional)
     - incident name (Required)
     - retry (Required)
     - active (Required)
    """

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")
    forgotten_keys = check_required_keys(request.json, ['component', 'name', 'url', 'method', 'incident', 'retry',
                                                        'active'])

    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json,
                                         ['component', 'name', 'url', 'method', 'status', 'incident', 'retry',
                                          'active'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    basic_vm: BasicHealthcheckVM = BasicHealthcheckVM(**request.json)
    basic: BasicHealthcheck = healthcheck_service.save_basic(basic_vm, dict(request.headers))

    headers = HeaderUtil.create_alert(APP_NAME, 'healthcheckManagement.saved', basic.name)
    return Response("", status=200, headers=headers)


@bp.route('/complex/save', methods=['POST'])
@RequiredContentType('application/json')
def save_complex():
    """
    body:
     - component (Required)
     - healthcheck name (Required)
     - code (Required) --> Code that runs in the healthcheck
     - retry (Required)
     - incident name (Required)
     - retry (Required)
     - active (Required)
    """

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['component', 'name', 'code', 'incident', 'retry', 'active'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(
                                      ', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['component', 'name', 'code', 'incident', 'retry', 'active'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    complex_vm: ComplexHealthcheckVM = ComplexHealthcheckVM(**request.json)
    complx: ComplexHealthcheck = healthcheck_service.save_complex(complex_vm, dict(request.headers))

    headers = HeaderUtil.create_alert(APP_NAME, 'healthcheckManagement.saved', complx.name)
    return Response("", status=200, headers=headers)


@bp.route('/basic/update', methods=['PUT'])
@RequiredContentType('application/json')
def update_basic():
    """
    Update the basic healthcheck with the specified id

    body:
     - id (Required)
     - component (Required)
     - healthcheck name (Required)
     - url (Required)
     - method (Required)
     - status code (Required)
     - incident name (Required)
     - retry (Required)
    :return:
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json,
                                         ['id', 'component', 'name', 'url', 'method', 'status', 'incident', 'retry'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json,
                                         ['id', 'component', 'name', 'url', 'method', 'status', 'incident', 'retry'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    basic_vm: BasicHealthcheckVM = BasicHealthcheckVM(**request.json)
    basic: BasicHealthcheck = healthcheck_service.update_basic(basic_vm)

    headers = HeaderUtil.create_alert(APP_NAME, 'healthcheckManagement.updated', basic.id)
    return Response(None, status=204, headers=headers)


@bp.route('/complex/update', methods=['PUT'])
@RequiredContentType('application/json')
def update_complex():
    """
    Update the complex healthcheck with the specified id

    body:
     - id (Required)
     - component (Required)
     - healthcheck name (Required)
     - code (Required)
     - incident name (Required)
     - retry (Required)
    :return:
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json,
                                         ['id', 'component', 'name', 'code', 'incident', 'retry'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json,
                                         ['id', 'component', 'name', 'code', 'incident', 'retry'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    complex_vm: ComplexHealthcheckVM = ComplexHealthcheckVM(**request.json)
    complx: ComplexHealthcheck = healthcheck_service.update_complex(complex_vm)

    headers = HeaderUtil.create_alert(APP_NAME, 'healthcheckManagement.updated', complx.id)
    return Response(None, status=204, headers=headers)


@bp.route('/basic/<identifier>', methods=['DELETE'])
def delete_basic(identifier):
    """
        Delete the basic healthcheck with the id specified in the url
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    basic: BasicHealthcheck = healthcheck_service.delete_basic(identifier)

    headers = HeaderUtil.create_alert(APP_NAME, 'healthcheckManagement.deleted', str(basic.id))
    return Response(None, status=204, headers=headers)


@bp.route('/complex/<identifier>', methods=['DELETE'])
def delete_complex(identifier):
    """
        Delete the complex healthcheck with the id specified in the url
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    complx: ComplexHealthcheck = healthcheck_service.delete_complex(identifier)

    headers = HeaderUtil.create_alert(APP_NAME, 'healthcheckManagement.deleted', str(complx.id))
    return Response(None, status=204, headers=headers)


@bp.route('/basic/<component>', methods=['GET'])
def get_basics_by_component(component):
    """
        Returns the basic healthchecks of the specified component
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {} (path: {})'.format(request.url_rule, request.method, request.path))

    basics = healthcheck_service.get_basic_healthchecks_by_component(component, dict(request.headers))

    # do a map to response with the following fields of each healthchecks
    tmp = list(map(lambda x: {'id': str(x.id), 'component': x.component, 'name': x.name, 'url': x.url,
                              'method': x.method, 'status': x.status, 'incident': x.incident, 'retry': x.retry},
                   basics))

    return jsonify(tmp)


@bp.route('/complex/<component>', methods=['GET'])
def get_complex_by_component(component):
    """
        Returns the complex healthchecks of the specified component
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {} (path: {})'.format(request.url_rule, request.method, request.path))

    complx = healthcheck_service.get_complex_healthchecks_by_component(component, dict(request.headers))

    # do a map to response with the following fields of each healthchecks
    tmp = list(map(lambda x: {'id': str(x.id), 'component': x.component, 'name': x.name, 'code': x.code,
                              'incident': x.incident, 'retry': x.retry}, complx))

    return jsonify(tmp)


@bp.route('/inStatus', methods=['POST'])
@RequiredContentType('application/json')
def instatus():
    """
    Returns the id of the component associated with the specified component name

    body:
     - Component name (Required) --> The name of the component associated with the healthcheck
    """

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['name'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(
                                      ', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['name'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    if data["name"].lower() == "website":
        return InStatusConfiguration.COMPONENTS[0].get('id')
    elif data["name"].lower() == "app":
        return InStatusConfiguration.COMPONENTS[1].get('id')
    else:
        raise BadRequestException(title='Component invalid',
                                  description='Unexpected component name: {}'.format(data["name"]))


@bp.route('/components/<component>', methods=['GET'])
def get_name_component_by_id(component):
    """
        Returns the name of the component specified associated with the healthcheck
    """

    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    if component.lower() == "ckmme0naq163998zvooo37ruawj":
        return InStatusConfiguration.COMPONENTS[0].get('name')
    elif component.lower() == "ckmme0nc2164008zvook07s10cq":
        return InStatusConfiguration.COMPONENTS[1].get('name')
    else:
        raise BadRequestException(title='Component invalid',
                                  description='Unexpected component id: {}'.format(component))


@bp.route('/components', methods=['GET'])
def get_components():
    """
        Returns all the components of the status page
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    if InStatusConfiguration.COMPONENTS is None:
        raise BadRequestException(title="No components available", description="There's no components on the response")
    else:
        return jsonify(InStatusConfiguration.COMPONENTS)


@bp.route('/incidents', methods=['GET'])
def get_incidents():
    """
        Returns all the incidents of the status page
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    data = instatus_service.get_incidents()

    return jsonify(data)


@bp.route('/statecomponents', methods=['GET'])
def get_componentss():
    """
        Returns all the components of the status page v2
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    data = instatus_service.get_components()

    return jsonify(data)


@bp.route('/instatus/incident', methods=['POST'])
@RequiredContentType('application/json')
def instatus_incident():
    """
    Call the function that creates incidents in the instatus service if the healthcheck fails (TEMPORAL)

    body:
     - healthcheck id (Required)
    """

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")
    forgotten_keys = check_required_keys(request.json, ['healthcheck_id'])

    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['healthcheck_id'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    # instatus_service.my_scheduled_job()
    response = instatus_service.healthcheck_with_incident.delay(data["healthcheck_id"])

    # instatus_service.probando.delay('primero', 'segundo')

    headers = HeaderUtil.create_alert(APP_NAME, 'instatusManagement.tested', response)
    return Response("", status=200, headers=headers)
