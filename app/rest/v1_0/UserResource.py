from logging import DEBUG

from flask import request, Response, jsonify

from app import APP_NAME, BadRequestException
from app.domain.User import User
from app.rest.v1_0 import bp
from app.rest.vm.ManagedUserVm import ManagedUserVM
from app.security.AccessDecorators import RequiredContentType
from app.services.UserService import UserService
from app.util.HeaderUtils import HeaderUtil, PaginationUtil
from app.util.JsonLogger import JsonLogger
from app.util.utils import check_required_keys, check_outside_keys
from config import PageConfiguration

user_service = UserService()


@bp.route('/users', methods=['POST'])
@RequiredContentType('application/json')
def create_user():
    """
    Create a user from the admin or system role

    body:
     - login (Required)
     - password (Optional)
     - firstName (Optional)
     - lastName (Optional)
     - email (Required)
     - activated (Required)
     - langKey (Required)
     - authorities (Optional)
    :return:
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['login', 'email', 'activated', 'langKey'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['login', 'password', 'firstName', 'lastName', 'email',
                                                        'activated', 'langKey', 'authorities'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    user_vm: ManagedUserVM = ManagedUserVM(**request.json)
    user: User = user_service.create_user(user_vm, dict(request.headers), activated=True)

    headers = HeaderUtil.create_alert(APP_NAME, 'userManagement.created', user.login)
    return Response("", status=201, headers=headers)


@bp.route('/users', methods=['PUT'])
@RequiredContentType('application/json')
def update_user():
    """
    Update the user with the specified id

    body:
     - id (Required)
     - login (Required)
     - password (Optional)
     - firstName (Optional)
     - lastName (Optional)
     - email (Required)
     - activated (Optional)
     - langKey (Required)
     - authorities (Optional)
    :return:
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['id', 'login', 'email', 'activated', 'langKey'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['id', 'login', 'password', 'firstName', 'lastName', 'email',
                                                        'activated', 'langKey', 'authorities', 'payment'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    user_vm: ManagedUserVM = ManagedUserVM(**request.json)
    user: User = user_service.update_user(user_vm, dict(request.headers))

    headers = HeaderUtil.create_alert(APP_NAME, 'userManagement.updated', user.login)
    return Response(None, status=204, headers=headers)


@bp.route('/users/<login>', methods=['DELETE'])
def delete_user(login):
    """
        Delete the user with the login specified in the url
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    user: User = user_service.delete_user(login)

    headers = HeaderUtil.create_alert(APP_NAME, 'userManagement.deleted', str(user.id))
    return Response(None, status=204, headers=headers)


@bp.route('/users', methods=['GET'])
def get_all_users():
    """
        Returns all the users
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))

    params = request.args.to_dict()

    page_num = int(params.get('page', 0))
    page_size = int(params.get('size', PageConfiguration.DEFAULT_SIZE))
    sort_column = params.get('sort', '').split(',')[0]
    sort_order = params.get('sort', '').split(',')[-1]

    page_info = user_service.get_all(page_num, page_size, sort_column, sort_order)

    headers = PaginationUtil.generate_pagination_http_headers(request.base_url, page_info)

    results = [item.transform() for item in page_info.results]

    response = jsonify(results)
    response.headers.update(headers)
    return response


@bp.route('/users/<login>', methods=['GET'])
def get_user(login):
    """
        Returns a user with a login specified in the url
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {} (login: {})'.format(request.url_rule, request.method, login))

    user: User = user_service.get_user(login)
    return jsonify(user.transform())


@bp.route('/users/login/<list:ids>', methods=['GET'])
def get_user_from_ids(ids):
    """
        Returns the users with the ids specified
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {} (path: {})'.format(request.url_rule, request.method, request.path))

    users = user_service.get_users_by_ids(ids)

    # do a map to response only the id and login of each user
    tmp = list(map(lambda x: {'id': str(x.id), 'login': x.login}, users))

    return jsonify(tmp)


@bp.route('/users/authorities', methods=['GET'])
def get_authorities():
    """
        Returns the different roles of the application
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {} (path: {})'.format(request.url_rule, request.method, request.path))

    authorities = user_service.get_authorities()

    return jsonify(authorities)
