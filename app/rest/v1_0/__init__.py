from flask import Blueprint

bp = Blueprint('api_v1_0', __name__)

from app.rest.v1_0 import UserResource
from app.rest.v1_0 import AccountResource
from app.rest.v1_0 import UserJwtController
from app.rest.v1_0 import HealthchecksResource
