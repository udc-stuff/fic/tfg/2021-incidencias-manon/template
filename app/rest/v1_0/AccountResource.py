from logging import DEBUG, INFO

from flask import request, Response, jsonify

from app import JsonLogger, BadRequestException, APP_NAME, executor
from app.domain.User import User
from app.errors.UnauthorizedException import UnauthorizedException
from app.rest.v1_0 import bp
from app.rest.vm.ChangePasswordVm import ChangePasswordVM
from app.rest.vm.KeyAndPasswordVm import KeyAndPasswordVM
from app.rest.vm.ManagedUserVm import ManagedUserVM
from app.security.AccessDecorators import RequiredContentType
from app.security.SecurityUtils import SecurityUtils
from app.services.MailService import MailService
from app.services.UserService import UserService
from app.util.HeaderUtils import HeaderUtil
from app.util.utils import check_required_keys, check_outside_keys

user_service = UserService()
mail_service = MailService()
security_utils = SecurityUtils()


def __check_password_length(password: str) -> bool:
    """
        Returns True if the password has a correct length
    """
    return password and ManagedUserVM.PASSWORD_MIN_LENGTH <= len(password) <= ManagedUserVM.PASSWORD_MAX_LENGTH


@bp.route('/register', methods=['POST'])
@RequiredContentType('application/json')
def register_account():
    """
    Register a new user in the database

    body:
     - login (Required)
     - password (Required)
     - firstName (Optional)
     - lastName (Optional)
     - email (Required)
     - activated (Optional)
     - langKey (Required)
     - authorities (Optional)
    :return:
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['login', 'email', 'password', 'langKey'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found following attributes: {}'.format(', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['login', 'password', 'firstName', 'lastName', 'email',
                                                        'activated', 'langKey', 'authorities'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    user_vm: ManagedUserVM = ManagedUserVM(**request.json)
    if not __check_password_length(user_vm.password):
        raise BadRequestException(title='Validation error',
                                  description='Password length must be between {} and {} (size: {})'
                                  .format(ManagedUserVM.PASSWORD_MIN_LENGTH, ManagedUserVM.PASSWORD_MAX_LENGTH,
                                          len(user_vm.password)))

    user: User = user_service.create_user(user_vm, dict(request.headers), activated=False)
    executor.submit(mail_service.send_activation_email,
                    user.email, user.activationKey, user.langKey)

    headers = HeaderUtil.create_alert(APP_NAME, 'userManagement.created', user.login)
    return Response("", status=201, headers=headers)


@bp.route('/activate', methods=['GET'])
def activate_account():
    """
        Activate the user account corresponding to the activation key
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {} args={}'.format(request.url_rule, request.method, request.args))

    params = request.args.to_dict()
    if 'key' not in params:
        raise BadRequestException(title='Argument not found', description='key argument is required')

    user_service.activate_account(params.get('key', ''))

    return Response(None, status=204)


@bp.route('/authenticate', methods=['GET'])
def is_authenticated():
    """
        Check if the user is authenticated and return its login
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    if not security_utils.isAuthenticated(dict(request.headers)):
        return 'Not Authenticated'

    login: str = security_utils.getCurrentLogin(dict(request.headers))

    JsonLogger.application(INFO, 'INFO LOGIN {}'.format(login))
    return login if login else ''


@bp.route('/account/change-password', methods=['POST'])
@RequiredContentType('application/json')
def change_password():
    """
    Change the password of the logged in user

    body:
     - currentPassword (Required)
     - newPassword (Required)
    :return:
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['currentPassword', 'newPassword'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found the following attributes: {}'.format(
                                      ', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['currentPassword', 'newPassword'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    change_password_vm: ChangePasswordVM = ChangePasswordVM(**request.json)

    if not __check_password_length(change_password_vm.newPassword):
        raise BadRequestException(title='Validation error',
                                  description='Password length must be between {} and {} (size: {})'
                                  .format(ManagedUserVM.PASSWORD_MIN_LENGTH, ManagedUserVM.PASSWORD_MAX_LENGTH,
                                          len(change_password_vm.newPassword)))

    user_service.change_password(change_password_vm.currentPassword, change_password_vm.newPassword,
                                 dict(request.headers))

    return Response(None, status=204)


@bp.route('/account', methods=['GET'])
def get_account():
    """
        Returns the following account information of the logged in user:

    - "activated":
    - "authorities":
    - "email":
    - "firstName":
    - "langKey":
    - "lastName":
    - "login":
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    identifier = security_utils.getCurrentUserId(dict(request.headers))
    if not identifier:
        return UnauthorizedException()

    data: ManagedUserVM = user_service.get_user_by_id_without_sensitive_data(identifier)

    return jsonify(data.__dict__)


@bp.route('/account/reset-password/init', methods=['POST'])
def request_password_reset():
    """
        Start the password reset process and return the resetKey in the response body
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    user: User = user_service.request_password_reset(request.data.decode("utf-8"))
    executor.submit(mail_service.send_password_reset_email,
                    user.email, user.resetKey, user.langKey)

    return Response(None, status=204)


@bp.route('/account/reset-password/finish', methods=['POST'])
@RequiredContentType('application/json')
def finish_password_reset():
    """
    Finish the process by changing the password for a new one of your choice

    body:
     - key (Required)
     - newPassword (Required)
    :return:
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    forgotten_keys = check_required_keys(request.json, ['key', 'newPassword'])
    if len(forgotten_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Not found the following attributes: {}'.format(
                                      ', '.join(forgotten_keys)))

    unexpected_keys = check_outside_keys(request.json, ['key', 'newPassword'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    key_and_password: KeyAndPasswordVM = KeyAndPasswordVM(**request.json)

    if not __check_password_length(key_and_password.newPassword):
        raise BadRequestException(title='Validation error',
                                  description='Password length must be between {} and {} (size: {})'
                                  .format(ManagedUserVM.PASSWORD_MIN_LENGTH, ManagedUserVM.PASSWORD_MAX_LENGTH,
                                          len(key_and_password.newPassword)))

    user_service.complete_password_reset(key_and_password.newPassword, key_and_password.key)

    return Response(None, status=204)


@bp.route('/account', methods=['POST'])
@RequiredContentType('application/json')
def save_account():
    """
    Update the following fields of the account information of the logged in user

    body:
     - firstName (Optional)
     - lastName (Optional)
     - langKey (Optional)
     - email (Optional)
    :return:
    """
    JsonLogger.application(DEBUG, 'RESOURCE {} {}'.format(request.url_rule, request.method))  # Application log

    data = request.get_json(silent=True)
    if data is None:
        raise BadRequestException(title="No data available", description="There's no data on the request")

    unexpected_keys = check_outside_keys(data, ['firstName', 'lastName', 'langKey', 'email'])
    if len(unexpected_keys) > 0:
        raise BadRequestException(title='Validation error',
                                  description='Unexpected attributes: {}'.format(', '.join(unexpected_keys)))

    user_service.save_account(data, dict(request.headers))

    return Response(None, 204)
