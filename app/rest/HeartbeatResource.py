from app.rest import bp


@bp.route('/heartbeat', methods=['GET'])
def get_heartbeat():
    return '', 204
