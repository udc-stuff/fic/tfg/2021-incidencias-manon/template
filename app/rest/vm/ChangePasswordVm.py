
class ChangePasswordVM:
    """View Model object to change a user's password"""

    currentPassword: str
    newPassword: str

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __str__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)

    def __repr__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)
