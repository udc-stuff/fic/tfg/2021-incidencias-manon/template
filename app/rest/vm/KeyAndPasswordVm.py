
class KeyAndPasswordVM:
    """View Model object for storing the user's key and password"""

    key: str
    newPassword: str

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __str__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)

    def __repr__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)
