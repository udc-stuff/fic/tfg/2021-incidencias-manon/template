from typing import List

from app.domain.User import User


class ManagedUserVM:
    """View Model the user, which is meant to be used in the user management UI"""

    id: str = None

    PASSWORD_MIN_LENGTH: int = 4
    PASSWORD_MAX_LENGTH: int = 100

    login: str  # required

    password: str = None

    firstName: str
    lastName: str

    email: str  # required

    activated: bool

    langKey: str

    authorities: List[str] = []

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def set_user_dto(self, user: User):
        self.password = ''

        self.login = user.login
        self.firstName = user.firstName
        self.lastName = user.lastName
        self.email = user.email
        self.activated = user.activated
        self.langKey = user.langKey
        self.authorities = user.authorities

    def get_user(self) -> User:
        u = User()

        if 'id' in self.__dict__:
            u.id = self.id

        u.login = self.login
        u.firstName = self.__dict__.get('firstName', '')
        u.lastName = self.__dict__.get('lastName', '')
        u.email = self.email
        u.activated = self.__dict__.get('activated', False)
        u.langKey = self.__dict__.get('langKey', 'en')

        u.authorities = self.__dict__.get('authorities', [])

        u.password = self.password

        return u

    def update_user(self, u: User) -> User:
        u.login = self.login

        u.firstName = self.__dict__.get('firstName', u.firstName)
        u.lastName = self.__dict__.get('lastName', u.lastName)
        u.email = self.__dict__.get('email', u.email)
        u.activated = self.__dict__.get('activated', u.activated)
        u.langKey = self.__dict__.get('langKey', u.langKey)

        u.authorities = self.__dict__.get('authorities', u.authorities)

        if self.password is not None or self.password != '':
            u.password = self.password

        return u

    def __str__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)

    def __repr__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)
