from app.domain.ComplexHealthcheck import ComplexHealthcheck


class ComplexHealthcheckVM:
    """View Model the complex healthcheck"""

    id: str = None

    component: str  # required

    name: str  # required

    code: str  # required

    incident: str  # required

    retry: bool  # required

    active: bool  # required

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def set_basic_dto(self, complex: ComplexHealthcheck):
        self.component = complex.component
        self.name = complex.name
        self.code = complex.code
        self.incident = complex.incident
        self.retry = complex.retry
        self.active = complex.active

    def get_basic(self) -> ComplexHealthcheck:
        c = ComplexHealthcheck()

        if 'id' in self.__dict__:
            c.id = self.id

        c.component = self.component
        c.name = self.name
        c.code = self.code
        c.incident = self.incident
        c.retry = self.retry
        c.active = self.active

        return c

    def update_complex(self, c: ComplexHealthcheck) -> ComplexHealthcheck:

        c.component = self.__dict__.get('component', c.component)
        c.name = self.__dict__.get('name', c.name)
        c.code = self.__dict__.get('code', c.code)
        c.incident = self.__dict__.get('incident', c.incident)
        c.retry = self.__dict__.get('retry', c.retry)
        c.active = self.__dict__.get('active', c.active)

        return c

    def __str__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)

    def __repr__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)
