from app.domain.BasicHealthcheck import BasicHealthcheck


class BasicHealthcheckVM:
    """View Model the basic healthcheck"""

    id: str = None

    component: str  # required

    name: str  # required

    url: str  # required

    method: str  # required

    status: int

    incident: str  # required

    retry: bool  # required

    active: bool  # required

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def set_basic_dto(self, basic: BasicHealthcheck):
        self.component = basic.component
        self.name = basic.name
        self.url = basic.url
        self.method = basic.method
        self.status = basic.status
        self.incident = basic.incident
        self.retry = basic.retry
        self.active = basic.active

    def get_basic(self) -> BasicHealthcheck:
        b = BasicHealthcheck()

        if 'id' in self.__dict__:
            b.id = self.id

        b.component = self.component
        b.name = self.name
        b.url = self.url
        b.method = self.method
        b.status = self.__dict__.get('status', '')
        b.incident = self.incident
        b.retry = self.retry
        b.active = self.active

        return b

    def update_basic(self, b: BasicHealthcheck) -> BasicHealthcheck:

        b.component = self.__dict__.get('component', b.component)
        b.name = self.__dict__.get('name', b.name)
        b.url = self.__dict__.get('url', b.url)
        b.method = self.__dict__.get('method', b.method)
        b.status = self.__dict__.get('status', b.status)
        b.incident = self.__dict__.get('incident', b.incident)
        b.retry = self.__dict__.get('retry', b.retry)
        b.active = self.__dict__.get('active', b.active)

        return b

    def __str__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)

    def __repr__(self) -> str:
        return "{}{}".format(self.__class__.__name__, self.__dict__)
