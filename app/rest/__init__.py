from flask import Blueprint

bp = Blueprint('api', __name__)

from app.rest import HealthcheckResource
from app.rest import HeartbeatResource
