import gevent  # its necessary right here to avoid errors (https://github.com/gevent/gevent/issues/1016)
from celery import Celery

if hasattr(gevent, 'monkey'):
    gevent.monkey.patch_all()

import sys
import re
import json
import flask
import traceback

from uuid import uuid4
from flask_mail import Mail
from flask_executor import Executor
from flask_cors import CORS
from flask import Flask, request, g
from flask_crontab import Crontab
from logging import DEBUG, INFO, ERROR
from timeit import default_timer as timer

from mongoengine import FieldDoesNotExist, ValidationError
from werkzeug.routing import BaseConverter
from werkzeug.exceptions import HTTPException

from config import BasicConfiguration, MailConfiguration
from app.util.JsonLogger import JsonLogger
from app.errors.BadRequestException import BadRequestException
from app.util.ErrorTemplate import CustomErrorException, ErrorTemplate

APP_NAME = __name__
app = None
api = None
celery_app = None

IN_CELERY_WORKER_PROCESS = sys.argv and sys.argv[0].endswith('celery') and 'worker' in sys.argv

crontab = Crontab()


# https://exploreflask.com/en/latest/views.html
class ListConverter(BaseConverter):
    """This class is to read a list in url parameters on a flask route"""

    def to_python(self, value):
        return [item for item in value.split(',') if item]

    def to_url(self, values):
        return ','.join(value for value in values)


def set_resources(app):
    """
    method to set the blueprint with the endpoint for the microservice

    :param app:
    :return:
    """
    from app.rest import bp as bp_api
    app.register_blueprint(bp_api, url_prefix='/')

    from app.rest.v1_0 import bp as bp_v1_0
    app.register_blueprint(bp_v1_0, url_prefix='/v1.0')


def make_celery(app):
    from config import CeleryConfiguration

    celery_app = Celery(app.name,
                        broker=CeleryConfiguration.BROKER,
                        backend=CeleryConfiguration.BACKEND,
                        include=CeleryConfiguration.INCLUDE)
    celery_app.conf.task_create_missing_queues = True
    celery_app.conf.update(app.config)
    celery_app.conf.update(
        task_serializer='json',
        accept_content=['json'],  # Ignore other content
        result_serializer='json',
        enable_utc=True,
        task_queues=CeleryConfiguration.TASK_QUEUES,
        task_routes=CeleryConfiguration.TASK_ROUTES,
        task_acks_late=True,
        worker_prefetch_multiplier=1,
    )

    class ContextTask(celery_app.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery_app.Task = ContextTask
    return celery_app


def create_app():
    """
    method to create the flask application

    :return:
    """

    # start db connection
    import app.repository

    application = Flask(APP_NAME)

    CORS(application, resources={r'/*': {'origins': '*'}})

    application.logger.setLevel(JsonLogger.level)

    for log in JsonLogger.logger.handlers:
        if log.__class__ == flask.logging.default_handler.__class__:
            application.logger.removeHandler(flask.logging.default_handler)
        application.logger.addHandler(log)

    # config mail
    application.config.update(
        MAIL_SERVER=MailConfiguration.SERVER,
        MAIL_PORT=MailConfiguration.PORT,
        MAIL_USE_SSL=MailConfiguration.USE_SSL,
        MAIL_USE_TLS=MailConfiguration.USE_TLS,
        MAIL_USERNAME=MailConfiguration.USERNAME,
        MAIL_PASSWORD=MailConfiguration.PASSWORD,
        MAIL_SUPPRESS_SEND=False,
        MAIL_DEBUG=False
    )

    application.url_map.converters['list'] = ListConverter
    application.url_map.strict_slashes = False

    celery_app = make_celery(application)
    application.celery = celery_app

    return application


def get_app():
    return app


def get_celery():
    return celery_app


app = create_app()
celery_app = app.celery
crontab.init_app(app)
mail = Mail(app)
executor = Executor(app)

if not IN_CELERY_WORKER_PROCESS:
    set_resources(app)


@app.before_first_request
def before_first_request_func():
    JsonLogger.application(DEBUG, 'Receive the first request')  # Application log


@app.before_request
def before_request_func():
    g.request_id = '-'.join([BasicConfiguration.APP_NAME, str(uuid4())])
    g.correlation_id = request.headers.get('X-Correlation-Id', g.request_id)
    g.start = timer()

    JsonLogger.request(INFO)  # Request log

    # Check if the request will be accepted or revoked
    from app.security.HttpSecurity import before_request_http_security
    before_request_http_security()


@app.teardown_request
def teardown_request(error=None):
    JsonLogger.application(DEBUG, 'Teardown request is running')  # Application log

    if error:
        JsonLogger.application(ERROR, str(error), traceback.format_exc())  # Application log


@app.errorhandler(CustomErrorException)
def handle_custom_exception(e):
    response = e.get_response()
    JsonLogger.response(INFO, response, e.title + '-' + e.detail, traceback.format_exc())  # Response log

    return response


@app.errorhandler(FieldDoesNotExist)
def handle_FieldDoesNotExist(e):
    """
    This is a mongoengine exception and it's launched when a request has extra parameters
    :param e:
    :return:
    """
    bad_request_exception = BadRequestException()
    error = ErrorTemplate(status=bad_request_exception.status,
                          title=bad_request_exception.title,
                          detail=bad_request_exception.detail,
                          path=request.path, exception=e)

    # Remove the exception message to remove internal names
    error.message = error.detail

    if len(e.args) == 1:
        tmp = re.findall(r'\{.*?\}', e.args[0])
        if tmp is not None and len(tmp) == 1:
            aux: str = tmp[0].replace('{', '(').replace('}', ')')
            error.detail = 'The request has  not allowed parameters {}'.format(aux)

    response = error.get_response()
    JsonLogger.response(INFO, response, error.title + '-' + error.detail, traceback.format_exc())  # Response log
    return response


@app.errorhandler(ValidationError)
def handle_ValidationError(e):
    """
    This is a mongoengine exception and it's launched when:
     -
    :param e:
    :return:
    """
    bad_request_exception = BadRequestException()
    error = ErrorTemplate(status=bad_request_exception.status,
                          title=bad_request_exception.title,
                          detail=bad_request_exception.detail,
                          path=request.path, exception=e)

    # Remove the exception message to remove internal names
    error.message = error.detail

    error.title = 'Validation error'
    error.detail = '.'.join(['{}: {}'.format(k, v) for k, v in e.to_dict().items()])

    response = error.get_response()
    JsonLogger.response(INFO, response, error.title + '-' + error.detail, traceback.format_exc())  # Response log
    return response


@app.errorhandler(Exception)
def handle_exception(e):
    error: ErrorTemplate
    level: int

    if isinstance(e, HTTPException):
        level = INFO
        error = ErrorTemplate(status=e.code,
                              title=e.name,
                              detail=e.description,
                              path=request.path, exception=e)

    else:
        level = ERROR
        error = ErrorTemplate(status=500,
                              title='caught non http-exception',
                              detail='an non http-exception was caught by flask and process by handle_exception',
                              path=request.path, exception=e)

    response = error.get_response()
    JsonLogger.response(level, response, error.title + '-' + error.detail, traceback.format_exc())  # Response log

    if level == ERROR:  # Not HTTP error, it is an error inside the server
        d = json.loads(response.get_data())
        # remove the message of the business login exception
        d['detail'] = d['message'] = \
            'The server encountered an unexpected condition that prevented it from fulfilling the request.'
        response.set_data(json.dumps(d))

    return response


@crontab.job(minute="*", hour="*", day="*", month="*", day_of_week="*")
def aux():
    from app.services.InstatusComunicationService import InstatusComunicationService

    try:
        comunationService = InstatusComunicationService()
        comunationService.my_scheduled_job()
        JsonLogger.application(DEBUG, 'Ha finalizado')
    except Exception as e:
        JsonLogger.application(DEBUG, 'Se ha lanzado una excepcion {}'.format(e.__class__.__name__),
                               traceback.format_exc())
