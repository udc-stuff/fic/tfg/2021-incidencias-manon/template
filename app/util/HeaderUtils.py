from app.util.PageInfo import PageInfo


class HeaderUtil:

    @staticmethod
    def create_alert(app_name: str, message: str, param: str) -> dict:
        return {
            'X-{}-alert'.format(app_name): message,
            'X-{}-params'.format(app_name): param
        }


class PaginationUtil:

    HEADER_X_TOTAL_COUNT: str = 'X-Total-Count'
    HEADER_LINK: str = 'Link'
    HEADER_LINK_FORMAT: str = '<{}>; rel="{}"'

    @staticmethod
    def __prepare_link(base_url: str, page_info: PageInfo, rel_type) -> str:
        """

        :param page_info:
        :param rel_type: str next | prev | last | first
        :return:
        """
        sort_column = 'id' if page_info.sort_column == '_id' else page_info.sort_column
        page_num: int
        if rel_type == 'next':
            page_num = page_info.page + 1
        elif rel_type == 'prev':
            page_num = page_info.page - 1
        elif rel_type == 'first':
            page_num = 0
        elif rel_type == 'last':
            page_num = page_info.total_pages - 1
        else:
            return ''

        link = '{}?sort={}%2C{}&page={}&size={}'.format(base_url, sort_column, page_info.sort_column, page_num, page_info.size)
        return PaginationUtil.HEADER_LINK_FORMAT.format(link, rel_type)

    @staticmethod
    def generate_pagination_http_headers(base_url, page_info: PageInfo) -> dict:

        link = ''
        if page_info.page < page_info.total_pages:
            link = link + PaginationUtil.__prepare_link(base_url, page_info, 'next') + ','
        if page_info.page > 0:
            link = link + PaginationUtil.__prepare_link(base_url, page_info, 'prev') + ','

        link = link + PaginationUtil.__prepare_link(base_url, page_info, 'last') + ','
        link = link + PaginationUtil.__prepare_link(base_url, page_info, 'first')

        return {
            PaginationUtil.HEADER_X_TOTAL_COUNT: page_info.total,
            PaginationUtil.HEADER_LINK: link
        }
