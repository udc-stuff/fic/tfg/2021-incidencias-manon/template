from typing import List


def check_required_keys(d: dict, keys: List[str]) -> List[str]:
    """
    This method return a list with the required that are not found in dictionary

    :param d: dictionary to check their keys
    :param keys: a list with the required keys
    :return: a list with the required keys not found on d
    """

    tmp = []
    for k in keys:
        if k not in d:
            tmp.append(k)

    return tmp


def check_outside_keys(d: dict, keys: List[str]) -> List[str]:
    """
    This method return a list with the dictionary keys that are not in keys

    :param d: dictionary to check their keys
    :param keys: a list with the expected keys
    :return: a list with the keys that should not be on d
    """

    tmp = []
    for k in d.keys():
        if k not in keys:
            tmp.append(k)

    return tmp
