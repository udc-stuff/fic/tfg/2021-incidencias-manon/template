import math
from typing import List

from app.repository.BaseRepository import PageDatabase


class PageInfo:
    page: int
    size: int
    sort_column: str
    sort_order: str

    total_pages: int

    results: List
    total: int

    def __init__(self, page: PageDatabase, results: List, total: int):
        self.page = page.page
        self.size = page.size
        self.sort_column = page.sort_column
        self.sort_order = 'desc' if page.sort_order == '-' else 'asc'

        self.results = results
        self.total = total

        self.total_pages = math.ceil(self.total / self.size)

        # Avoid have page 10000 when size = 10 and total = 15 (should be 2)
        self.page = min(self.page, self.total_pages)
