import errno
import os
import json
import flask
import logging
import platform
import hashlib
import traceback

from datetime import datetime
from tzlocal import get_localzone
from timeit import default_timer as timer

from logging.handlers import TimedRotatingFileHandler


class ContextFilter(logging.Filter):
    """
    Enhances log messages with contextual information
    This context add request_id and correlation_id if exist a flask request context
    """
    def filter(self, record: logging.LogRecord) -> int:
        """
        this filter adds on logging:
         - request_id
         - correlation_id (for trace)
        modify:
          - pathname: remove the directories before the main project folder
          - msg: if seem have a json format remove brackets to add all on same level
        :param record:
        :return:
        """

        # Remove brackets to include all on the same level
        if record.msg.startswith('{') and record.msg.endswith('}'):
            record.msg = record.msg[1:-1]

        # Remove the path outside the project
        root_dir = os.path.abspath(os.getcwd())
        if record.pathname.startswith(root_dir):
            record.pathname = record.pathname[len(root_dir)+1:]

        if flask.has_request_context():
            record.rid = getattr(flask.g, 'request_id', '')
            record.cid = getattr(flask.g, 'correlation_id', '')

            # if flask.request.headers.get('X-Real-Ip'):
            #     record.ip = flask.request.get('X-Real-Ip')
            # elif flask.request.headers.getlist("X-Forwarded-For"):
            #     record.ip = record.remote_addr = flask.request.headers.getlist("X-Forwarded-For")[0]
            # else:
            #     record.ip = flask.request.remote_addr
        else:
            record.rid = ''
            record.cid = ''
            # record.ip = ''

        return True


class JsonLogger:

    logger = None
    level = logging.INFO
    is_stream = True
    formatter = logging.Formatter("%(message)s")
    pathname = None

    # required keywords to instance JsonLogger
    keywords = ('logger_name', 'app_name', 'app_version', 'pathname')

    """singelton implementation for this class"""
    def __new__(cls, *args, **kwargs):
        """
        :param args: not used
        :param kwargs: the follower parameters are used
            :param logger_name: the name of the logger to use
            :param level: level of log to show the record
            :param pathname: pathname where log will be save  (file log always saved)
            :param stream: True if use stdout to show logs
            :param app_name: the name of the app which save the log
            :param app_version: the version of the which save the log
        """

        if '__instance' in cls.__dict__:
            return cls.__instance

        if not all(name in kwargs for name in cls.keywords):
            raise SystemExit('{} required the following kwargs: {}'.format(__name__, cls.keywords))

        cls.__instance = super(JsonLogger, cls).__new__(cls)
        cls.logger_name = kwargs.get('logger_name', 'JsonLogger')
        cls.level = kwargs.get('level', cls.level)
        cls.is_stream = kwargs.get('stream', cls.is_stream)

        cls.app_name = kwargs.get('app_name')
        cls.app_version = kwargs.get('app_version')
        cls.pathname = kwargs.get('pathname')

        cls.component_version = '{}:{}'.format(cls.app_name, cls.app_version)
        cls.component_name = hashlib.md5(cls.component_version.encode("utf-8")).hexdigest()

        cls.formatter = logging.Formatter(
            "{'written_at': '%(asctime)s', "
            "'correlation_id': '%(cid)s', 'request_id': '%(rid)s', "
            "'component_id': '" + cls.component_name + "', 'component_name': '" + cls.component_version + "', "
            "'component_instance': '" + platform.node() + "', "
            "'logger': '%(name)s', 'level': '%(levelname)s', "
            "'process': '%(processName)s', 'thread': '%(threadName)s', "
            "'filename': '%(pathname)s', 'module': '%(module)s', 'function': '%(funcName)s', 'line': '%(lineno)d', "
            "%(message)s}",
            datefmt="%Y-%m-%dT%H:%M:%S%z"
        )

        cls.logger = logging.getLogger(cls.logger_name)
        cls.logger.setLevel(cls.level)

        cls.__add_stream_handler()
        cls.__add_time_rotating_file_handler()

        return cls.__instance

    @classmethod
    def __add_stream_handler(cls):
        # add stream handler
        if cls.is_stream:
            stream_handler = logging.StreamHandler()
            stream_handler.addFilter(ContextFilter())
            stream_handler.setFormatter(cls.formatter)
            stream_handler.setLevel(cls.level)
            cls.logger.addHandler(stream_handler)

    @classmethod
    def __add_time_rotating_file_handler(cls):
        # add time rotating file
        log_dir = os.path.dirname(cls.pathname)
        try:
            if not os.path.exists(log_dir):
                os.makedirs(log_dir, exist_ok=True)

            file_handler = TimedRotatingFileHandler(cls.pathname, when='D', interval=1, backupCount=30, utc=True)
            file_handler.addFilter(ContextFilter())
            file_handler.setLevel(cls.level)
            file_handler.setFormatter(cls.formatter)
            cls.logger.addHandler(file_handler)

            cls.application(logging.DEBUG, 'Add TimedRotatingFileHandler on logger (pathname={})'.format(cls.pathname))

        except OSError as e:
            if e.errno != errno.EEXIST:
                cls.application(logging.CRITICAL,
                                'Cannot create the directory for the logs ({}) check permissions'.format(log_dir),
                                traceback.format_exc())

    def __init__(self, *args, **kwargs):
        pass

    @classmethod
    def __save(cls, level, type, **kwargs):
        # add common values
        record = {'type': type}

        # get the specific values
        record.update(kwargs.copy())

        cls.logger.log(level, json.dumps(record), stacklevel=3)

    @classmethod
    def application(cls, level, message: str, exc_info=None):
        data = {'msg': message}
        if exc_info is not None:
            data['exc_info'] = exc_info

        cls.__save(level, 'log', **data)

    @classmethod
    def request(cls, level):
        cls.__save(level, 'request',
                   request=flask.request.base_url, request_received_at=datetime.now(get_localzone()).isoformat(),
                   protocol=flask.request.environ.get('SERVER_PROTOCOL'), method=flask.request.method,
                   remote_ip=flask.request.remote_addr,  # Change to get a custom
                   request_size_b=flask.request.content_length or 0,
                   request_content_type=flask.request.content_type or '',
                   user_agent=flask.request.headers.get('User-Agent', ""),
                   referrer=flask.request.headers.get("Referer") or '',
                   x_forwarded_for=flask.request.headers.getlist("X-Forwarded-For") or '',
                   x_real_ip=flask.request.headers.get("X-Real-Ip") or ''
                   )

    @classmethod
    def response(cls, level, response, msg=None, exc_info=None):
        # cannot be 100% sure that after_request works so if g.end do not exist a timer() will be called
        end = getattr(flask.g, 'end', timer())
        diff_time = int((end - flask.g.start) * 1000)  # to get a time in ms

        data = {
            'request': flask.request.base_url,
            'method': flask.request.method,
            'response_status': response.status_code,
            'response_sent_at': datetime.now(get_localzone()).isoformat(),
            'response_time_ms': diff_time,
            'response_size_b': response.content_length,
            'response_content_type': response.content_type
        }

        if msg is not None:
            data['msg'] = msg
        if exc_info is not None:
            data['exc_info'] = exc_info

        cls.__save(level, 'response', **data)
