from datetime import datetime
from http.client import responses
from flask import jsonify, g
from tzlocal import get_localzone

from config import BasicConfiguration


class ErrorTemplate:

    def __init__(self, status: int, title: str, detail: str, path: str, exception: Exception, headers: dict = {}):
        """
        Template to build the response for the errors produces on the api

        :param status: An http status
        :param title: A brief human-readable message
        :param detail: A lengthier explanation of the error
        :param path: A base url
        :param exception: Which exception launch the error
        :param headers: A dictionary of headers to add on the response
        """
        self.timestamp = datetime.now(get_localzone()).isoformat()
        self.status = status
        self.error = responses[status]
        self.exception = exception.__class__.__name__
        self.message = repr(exception)
        self.title = title
        self.detail = detail
        self.path = path
        self.app_version = BasicConfiguration.VERSION
        self.trace_id = g.correlation_id
        self.headers = headers

    def get_response(self):
        """
        Method to generate a flask response with the error

        :return: flask response for the error
        """
        data = self.__dict__.copy()
        data.pop('headers', '')  # headers shouldn't be on the body

        response = jsonify(data)
        response.status_code = self.status

        for key, value in self.headers.items():
            response.headers.set(key, value)

        return response


class CustomErrorException(Exception):

    def __init__(self, flask_request, status: int, title: str, detail: str, headers: dict = {}):
        """
        Custom Exception to handle services exceptions

        :param flask_request: object with the flask request
        :param status: http status for the response
        :param title: A brief human-readable message
        :param detail: A lengthier explanation of the error
        :param headers: A dictionary of headers to add on the response
        """
        self.request = flask_request
        self.status = status
        self.title = title
        self.detail = detail
        self.headers = headers

    def get_response(self):
        """
        Method to generate a flask response with the error

        :return: flask response for the error
        """
        error: ErrorTemplate = ErrorTemplate(self.status, self.title, self.detail, self.request.base_url,
                                             self, self.headers)
        return error.get_response()

    def add_headers(self, headers: dict):
        self.headers.update(headers)
