import json
from mongoengine import Document, StringField


class Authority(Document):

    name = StringField(required=True, max_length=50, unique=True)
    meta = {
        'indexes': [
            'name'
        ]
    }

    def transform(self):
        return json.loads(self.to_json())
