import json
from datetime import datetime, timezone

from werkzeug.security import check_password_hash, generate_password_hash

from mongoengine import Document, StringField, DateTimeField, ListField, EmailField, BooleanField


class User(Document):
    login = StringField(required=True, max_length=100, unique=True)
    password = StringField(required=True, max_length=500, default='')

    firstName = StringField(max_length=200)
    lastName = StringField(max_length=200)

    email = EmailField(required=True, max_length=100, unique=True)

    activated = BooleanField(required=True)

    langKey = StringField(required=True, max_length=10)

    createdBy = StringField(max_length=100)
    createdDate = DateTimeField(required=True)

    authorities = ListField(StringField(max_lenght=50))

    activationKey = StringField(max_length=200)
    resetKey = StringField(max_length=200)
    resetDate = DateTimeField()

    lastModifiedBy = StringField(max_length=100)
    lastModifiedDate = DateTimeField()

    meta = {
        'indexes': [
            'login', 'email'
        ]
    }

    def transform(self) -> dict:
        """
        This function transform a Document entity to dictionary
        :return:
        """
        d = json.loads(self.to_json())

        if '_id' in d:
            d['id'] = d['_id']['$oid']
            d.pop('_id')

        if d.get('createdDate', '') != '':
            d['createdDate'] = self.createdDate.isoformat()

        if d.get('resetDate', '') != '':
            d['resetDate'] = self.resetDate.isoformat()
        if d.get('lastModifiedDate', '') != '':
            d['lastModifiedDate'] = self.lastModifiedDate.isoformat()

        return d

    def set_password(self, password):
        if password is not None:
            self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def save(self, *args, **kwargs):
        if self.id is None:
            self.createdDate = datetime.now(timezone.utc)

        return super(User, self).save(*args, **kwargs)
