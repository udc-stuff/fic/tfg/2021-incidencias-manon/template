import json
from mongoengine import Document, StringField, IntField, BooleanField


class BasicHealthcheck(Document):
    component = StringField(required=True, max_length=500)

    name = StringField(required=True, max_length=300)

    url = StringField(required=True, max_length=500)

    method = StringField(required=True, max_length=10, default='')

    status = IntField(max_length=4)

    incident = StringField(required=True, max_length=300)

    retry = BooleanField(required=True)

    active = BooleanField(required=True)

    meta = {
        'indexes': [
            'name', 'component', 'url'
        ]
    }

    def transform(self):
        """
            This function transform a Document entity to dictionary
            :return:
        """

        d = json.loads(self.to_json())

        if '_id' in d:
            d['id'] = d['_id']['$oid']
            d.pop('_id')

        return d

    def save(self, *args, **kwargs):
        return super(BasicHealthcheck, self).save(*args, **kwargs)
