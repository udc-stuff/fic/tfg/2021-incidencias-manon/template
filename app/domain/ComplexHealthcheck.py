import json
from mongoengine import Document, StringField, BooleanField


class ComplexHealthcheck(Document):
    component = StringField(required=True, max_length=500)

    name = StringField(required=True, max_length=300)

    code = StringField(required=True, max_length=5000)

    incident = StringField(required=True, max_length=300)

    retry = BooleanField(required=True)

    active = BooleanField(required=True)

    meta = {
        'indexes': [
            'component', 'code'
        ]
    }

    def transform(self):
        return json.loads(self.to_json())

    def save(self, *args, **kwargs):
        return super(ComplexHealthcheck, self).save(*args, **kwargs)
