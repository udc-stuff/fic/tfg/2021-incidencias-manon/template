from flask import request

from app.util.ErrorTemplate import CustomErrorException


class UnauthorizedException(CustomErrorException):

    msg = 'You are not authenticated–either not authenticated at all or authenticated incorrectly–but ' \
          'please reauthenticate and try again'

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Unauthorized request'
        self.detail = description or self.msg
        super(UnauthorizedException, self) \
            .__init__(request, 401, self.title, self.detail)

    def __repr__(self):
        return 'Client error status response code indicates that the request has not been applied ' \
               'because it lacks valid authentication credentials for the target resource'
