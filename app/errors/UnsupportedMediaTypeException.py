from flask import request

from app.util.ErrorTemplate import CustomErrorException


class UnsupportedMediaTypeException(CustomErrorException):

    msg = "The server refuses to accept the request because the payload format is in an unsupported format. " \
          "The format problem might be due to the request's indicated Content-Type or Content-Encoding, " \
          "or as a result of inspecting the data directly."

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Unsupported media type'
        self.detail = description or self.msg
        super(UnsupportedMediaTypeException, self) \
            .__init__(request, 415, self.title, self.detail)

    def __repr__(self):
        return self.msg
