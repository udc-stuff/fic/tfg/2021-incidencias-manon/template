import flask
from flask import request

from app.util.ErrorTemplate import CustomErrorException
from config import BasicConfiguration


class NotImplementedException(CustomErrorException):

    msg = 'I am so embarrassed. ' \
          'This message appears when a resource has started to be created but has not been completed. ' \
          'This message should not appear in production. ' \
          'If you read this message I would ask you to send the entire body of the response to the email {mail}. '\
          'Your email will allow us to fix this as soon as possible and we will be very grateful. '\
          '(endpoint={endpoint}, correlation_id={cid})'

    def __init__(self, description=None):
        self.endpoint = request.endpoint
        self.correlation_id = 'Unknown'

        self.detail = description
        if self.detail is None:
            if flask.has_request_context():
                self.correlation_id = getattr(flask.g, 'correlation_id', '')

            self.detail = self.msg\
                .format(mail=BasicConfiguration.SUPPORT_MAIL, endpoint=self.endpoint, cid=self.correlation_id)

        super(NotImplementedException, self)\
            .__init__(request, 501, 'Method not implemented', self.detail)

    def __repr__(self):
        return 'The method is not implemented'
