from flask import request

from app.util.ErrorTemplate import CustomErrorException


class PayloadTooLargeException(CustomErrorException):

    msg = "The request entity is too large"

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Payload too large'
        self.detail = description or self.msg
        super(PayloadTooLargeException, self) \
            .__init__(request, 413, self.title, self.detail)

    def __repr__(self):
        return self.msg
