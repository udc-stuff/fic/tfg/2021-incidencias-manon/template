from flask import request

from app.util.ErrorTemplate import CustomErrorException


class UnprocessableEntityException(CustomErrorException):

    msg = 'The server understands the content type of the request entity, ' \
          'and the syntax of the request entity is correct, ' \
          'but it was unable to process the contained instructions'

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Unprocessable Entity'
        self.detail = description or self.msg
        super(UnprocessableEntityException, self) \
            .__init__(request, 422, self.title, self.detail)

    def __repr__(self):
        return self.msg
