from flask import request

from app.util.ErrorTemplate import CustomErrorException


class InternalServerErrorException(CustomErrorException):

    msg = "The server has encountered a situation it doesn't know how to handle"

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Internal Server Error'
        self.detail = description or self.msg
        super(InternalServerErrorException, self) \
            .__init__(request, 500, self.title, self.detail)

    def __repr__(self):
        return self.msg
