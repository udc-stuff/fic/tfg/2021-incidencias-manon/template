from flask import request

from app import CustomErrorException


class PaymentRequiredException(CustomErrorException):

    msg = 'Payment required code indicates that the server can handle the request but the client exceeded the credits'

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Payment Required'
        self.detail = description or self.msg

        super(PaymentRequiredException, self) \
            .__init__(request, 402, self.title, self.detail)

    def __repr__(self):
        return self.msg
