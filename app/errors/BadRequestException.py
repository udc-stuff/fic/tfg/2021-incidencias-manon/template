from flask import request

from app.util.ErrorTemplate import CustomErrorException


class BadRequestException(CustomErrorException):

    _repr = "The server cannot process the request due to something that is perceived to be a client error " \
             "(e.g., malformed request syntax, invalid request message framing, or deceptive request routing)."

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Bad request'
        self.detail = description or 'The client has been unable to communicate satisfactorily with the web host'
        super(BadRequestException, self) \
            .__init__(request, 400, self.title, self.detail)

    def __repr__(self):
        return self._repr
