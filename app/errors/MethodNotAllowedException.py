from typing import List

from flask import request

from app.util.ErrorTemplate import CustomErrorException


class MethodNotAllowedException(CustomErrorException):

    msg = 'The request method is known by the server but is not supported by the target resource.'

    def __init__(self, methods: List[str], title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Method not allowed'
        self.detail = description or self.msg

        self.headers = {'Allow': ','.join(methods)}

        super(MethodNotAllowedException, self) \
            .__init__(request, 405, self.title, self.detail, self.headers)

    def __repr__(self):
        return "The client does not have access rights to the content; that is, " \
               "it is unauthorized, so the server is refusing to give the requested resource. " \
               "Unlike 401, the client's identity is known to the server."
