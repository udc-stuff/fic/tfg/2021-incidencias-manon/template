from flask import request

from app.util.ErrorTemplate import CustomErrorException


class NotFoundException(CustomErrorException):

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Path not found: {}'.format(request.path)
        self.detail = description or 'The requested resource was not found on this server'
        super(NotFoundException, self) \
            .__init__(request, 404, self.title, self.detail)

    def __repr__(self):
        return 'The method received in the request-line is known by the origin server but not supported by the target resource.'
