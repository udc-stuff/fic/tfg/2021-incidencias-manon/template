from flask import request

from app.util.ErrorTemplate import CustomErrorException


class TooManyRequestsException(CustomErrorException):

    def __init__(self, description: str, max_request: int, reset: float):
        self.endpoint = request.endpoint

        self.detail = description

        self.headers = {
            'X-RateLimit-Limit': max_request,
            'X-RateLimit-Remaining': 0,
            'X-RateLimit-Reset': reset
        }

        super(TooManyRequestsException, self)\
            .__init__(request, 429, 'Rate Limit exceed', self.detail, self.headers)

    def __repr__(self):
        return 'The user has sent too many requests in a given amount of time ("rate limiting").'
