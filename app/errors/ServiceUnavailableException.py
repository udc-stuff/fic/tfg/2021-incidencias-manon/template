from flask import request

from app import CustomErrorException


class ServiceUnavailableException(CustomErrorException):

    msg = 'server error response code indicates that the server is not ready to handle the request. ' \
          'Common causes are a server that is down for maintenance or that is overloaded.'

    def __init__(self, title=None, description=None, retry_after=None):
        self.endpoint = request.endpoint
        self.title = title or 'Service Unavailable'
        self.detail = description or self.msg

        self.headers = {}
        if not(retry_after is None):
            self.headers['Retry-After'] = retry_after

        super(ServiceUnavailableException, self) \
            .__init__(request, 503, self.title, self.detail, headers=self.headers)

    def __repr__(self):
        return self.msg
