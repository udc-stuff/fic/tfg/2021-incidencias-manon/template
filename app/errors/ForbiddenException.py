from flask import request

from app.util.ErrorTemplate import CustomErrorException


class ForbiddenException(CustomErrorException):

    msg = 'I am sorry. ' \
          'I know who you are – I believe who you say you are – ' \
          'but you just don’t have permission to access this resource. ' \
          'Maybe if you ask the system administrator nicely, you will get permission. ' \
          'But please do not bother me again until your predicament changes.'

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Forbidden request'
        self.detail = description or self.msg
        super(ForbiddenException, self) \
            .__init__(request, 403, self.title, self.detail)

    def __repr__(self):
        return "The client does not have access rights to the content; that is, " \
               "it is unauthorized, so the server is refusing to give the requested resource. " \
               "Unlike 401, the client's identity is known to the server."
