from flask import request

from app.util.ErrorTemplate import CustomErrorException


class LengthRequiredException(CustomErrorException):

    msg = "The server refuses to accept the request without a defined Content-Length header."

    def __init__(self, title=None, description=None):
        self.endpoint = request.endpoint
        self.title = title or 'Length required'
        self.detail = description or self.msg
        super(LengthRequiredException, self) \
            .__init__(request, 411, self.title, self.detail)

    def __repr__(self):
        return self.msg
