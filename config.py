import os
import logging
import platform
from typing import List
from kombu import Queue, Exchange

from app.security.AuthoritiesConstants import AuthoritiesConstants
from app.util.JsonLogger import JsonLogger


class BasicConfiguration:
    APP_NAME: str = 'APPLICATION NAME'
    VERSION: str = '0.1'
    BASE_URL: str = os.getenv('BASIC_BASE_URL', 'http://localhost')
    SUPPORT_MAIL: str = os.getenv('BASIC_SUPPORT_MAIL', 'notify@mail.com')
    NODE: str = platform.node()

    LOG_DIR: str = os.getenv('BASIC_LOG_DIR', '/tmp/log/{}'.format(APP_NAME))
    LOG_FILE: str = '{}/{}-{}.log'.format(LOG_DIR, NODE, APP_NAME)
    LOGGER_LEVEL: str = os.getenv('BASIC_LOGGER_LEVEL', 'DEBUG')

    """singleton implementation for this class"""

    def __new__(cls, *args, **kwargs):
        try:
            cls.__instance
        except AttributeError:
            cls.__instance = super(BasicConfiguration, cls).__new__(cls)
        return cls.__instance

    def __init__(self):
        tmp_level = {
            'DEBUG': logging.DEBUG, 'INFO': logging.INFO, 'WARNING': logging.WARNING,
            'ERROR': logging.ERROR, 'CRITICAL': logging.CRITICAL
        }

        # New version of log
        JsonLogger(logger_name='logger_name', level=tmp_level.get(self.LOGGER_LEVEL, 'DEBUG'),
                   pathname=self.LOG_FILE, stream=True, app_name=self.APP_NAME, app_version=self.VERSION)


class MailConfiguration:
    USERNAME: str = os.getenv('MAIL_USERNAME', 'username')
    PASSWORD: str = os.getenv('MAIL_PASSWORD', 'password')
    SERVER: str = os.getenv('MAIL_SERVER', '')
    PORT: int = int(os.getenv('MAIL_PORT', '0'))

    USE_SSL: bool = os.getenv('MAIL_USE_SSL', '').lower() == 'true'
    USE_TLS: bool = os.getenv('MAIL_USE_TLS', '').lower() == 'true'

    TEMPLATE_DIR = 'resources/templates/mail'
    BASE_URL = BasicConfiguration.BASE_URL

    APP_NAME = BasicConfiguration.APP_NAME


class SecurityConfiguration:
    __JWT_SECRET = os.getenv('JWT_SECRET', 's3cr3t')

    # path for front-end application
    WEB_IGNORE = []

    # path for permit all request
    # url     -> endpoint
    # methods -> list of allowed methods
    # auth    -> False not required an authentication token
    #         -> True required a valid authentication token (jwt)
    # role    -> Optional. If is indicated the authentication must contain this role
    AUTHORIZE_REQUEST = [
        # versioned api
        {'url': '/v.*/fake', 'methods': ['GET'], 'auth': False},
        {'url': '/v.*/users', 'methods': ['POST', 'PUT', 'DELETE'], 'auth': True, 'role': AuthoritiesConstants.ADMIN},
        {'url': '/v.*/users/.*', 'methods': ['DELETE'], 'auth': True, 'role': AuthoritiesConstants.ADMIN},
        {'url': '/v.*/users', 'methods': ['GET'], 'auth': True},
        {'url': '/v.*/users/.*', 'methods': ['GET'], 'auth': True},
        {'url': '/v.*/users/login.*', 'methods': ['GET'], 'auth': True, 'role': AuthoritiesConstants.ADMIN},
        {'url': '/v.*/users/authorities', 'methods': ['GET'], 'auth': True, 'role': AuthoritiesConstants.ADMIN},
        {'url': '/v.*/register', 'methods': ['POST'], 'auth': False},
        {'url': '/v.*/activate', 'methods': ['GET'], 'auth': False},
        {'url': '/v.*/authenticate', 'methods': ['GET'], 'auth': False},
        {'url': '/v.*/account/reset-password/init', 'methods': ['POST'], 'auth': False},
        {'url': '/v.*/account/reset-password/finish', 'methods': ['POST'], 'auth': False},
        {'url': '/v.*/authenticate', 'methods': ['POST'], 'auth': False},
        {'url': '/v.*', 'methods': ['GET', 'POST', 'PUT', 'DELETE'], 'auth': True},
        {'url': '/v.*', 'methods': ['OPTIONS'], 'auth': False},
        # health check
        {'url': '/healthcheck', 'methods': ['GET'], 'auth': False},
        # heartbeat
        {'url': '/heartbeat', 'methods': ['GET'], 'auth': False}
    ]

    REJECT_NOT_MATCHED = True

    def __init__(self):
        JsonLogger.application(logging.DEBUG, 'starting security configuration')

        from app.security.jwt.TokenProvider import TokenProvider
        TokenProvider(private_key=self.__JWT_SECRET)


class PageConfiguration:
    DEFAULT_SIZE: int = 20
    MIN_SIZE: int = 1
    MAX_SIZE: int = 50


class DatabaseConfiguration:
    CREATE_ENTITIES: bool = os.getenv('CREATE_ENTITIES', 'False').lower() == 'true'
    REQUIRED: bool = os.getenv('DATABASE_REQUIRED', 'True').lower() == 'true'
    USERNAME: str = os.getenv('DATABASE_USERNAME', 'myUserAdmin')
    PASSWORD: str = os.getenv('DATABASE_PASSWORD', 'admin')
    SERVER: str = os.getenv('DATABASE_SERVER', 'localhost')
    PORT: int = int(os.getenv('DATABASE_PORT', '0'))  # 0 means use default
    DATABASE: str = os.getenv('DATABASE_DATABASE', 'mydb')
    QUERY_CONFIGURATION: str = os.getenv('DATABASE_QUERY_CONFIGURATION', '?authSource=admin')
    PROTOCOL: str = os.getenv('DATABASE_PROTOCOL', 'mongodb')  # should use mongomock on test environments


class InStatusConfiguration:
    API_KEY: str = os.getenv('INSTATUS_APIKEY', '4c9bcfd71b2da84d670768aea2d08eaa')
    PAGE_ID: str = os.getenv('INSTATUS_PAGEID', 'ckmme0n8v163974zvooauy9iu9u')
    COMPONENTS: List[dict] = [{'name': 'website', 'id': 'ckmme0naq163998zvooo37ruawj'},
                              {'name': 'app', 'id': 'ckmme0nc2164008zvook07s10cq'}]


class CeleryConfiguration:
    BROKER: str = os.getenv('CELERY_BROKER',
                            'amqp://rabbitmq:5672')
    BACKEND: str = os.getenv('CELERY_BACKEND',
                             'rpc://rabbitmq:5672')
    INCLUDE: List[str] = [
        'app.services.InstatusComunicationService',
        'app.rest.v1_0.HealthchecksResource'
    ]

    TASK_QUEUES = {
        'healthcheck': {'exchange': 'healthcheck', 'routing_key': 'healthcheck'},
        'mail': {'exchange': 'mail', 'routing_key': 'mail'}
    }

    TASK_ROUTES = {
        'app.service.instatus_service.healthcheck_with_incident': 'healthcheck',
        'app.service.mail_service.send_mail': 'mail',
    }


# load basic configuration
BasicConfiguration()
SecurityConfiguration()
